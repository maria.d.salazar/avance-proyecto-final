/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Mary Salazar
 */
public class Validaciones {

    /**
     * Recibimos parametros de la vista para validar los txt
     *
     * @param num
     * @param txt
     * @param tam
     */
    public void valNum(KeyEvent num, JTextField txt, int tam) {
        try {
            char c = num.getKeyChar();
            if (!Character.isDigit(c)) {
                num.consume();
            }
            if (txt.getText().length() == tam) {
                num.consume();
                Toolkit.getDefaultToolkit().beep();

            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "Error");
        }
    }

    /**
     * Recibimos parametros de la vista para validar los txt
     *
     * @param num
     * @param txt
     * @param tam
     */
    public void valLetr(KeyEvent num, JTextField txt, int tam) {
        try {
            char c = num.getKeyChar();
            if (Character.isDigit(c)) {
                num.consume();
            }
            if (txt.getText().length() == tam) {
                num.consume();
                Toolkit.getDefaultToolkit().beep();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "Error");
        }
    }

    /**
     * Recibimos parametros de la vista para validar los txt - Angel Román
     *
     * @param num
     * @param txt
     * @param tam
     */
    public void valDec(KeyEvent num, JTextField txt, int tam) {
        try {
            char c = num.getKeyChar();
            if (((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE) && (c != '.' || txt.getText().contains("."))) {
                num.consume();
            }
            if (txt.getText().length() == tam) {
                num.consume();
                Toolkit.getDefaultToolkit().beep();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "Error");
        }
    }

    /**
     * Permite verificar que la cedula ingresada es valida a nivel de Ecuador
     *
     * @param cedula
     * @param txt
     * @return
     */
    public boolean validadorDeCedula(String cedula, JTextField txt) {
        boolean cedulaCorrecta = false;
        try {
            if (cedula.length() == 10) // ConstantesApp.LongitudCedula
            {
                int tercerDigito = Integer.parseInt(cedula.substring(2, 3));
                if (tercerDigito < 6) {
                    // Coeficientes de validación cédula
                    // El decimo digito se lo considera dígito verificador
                    int[] coefValCedula = {2, 1, 2, 1, 2, 1, 2, 1, 2};
                    int verificador = Integer.parseInt(cedula.substring(9, 10));
                    int suma = 0;
                    int digito = 0;
                    for (int i = 0; i < (cedula.length() - 1); i++) {
                        digito = Integer.parseInt(cedula.substring(i, i + 1)) * coefValCedula[i];
                        suma += ((digito % 10) + (digito / 10));
                    }
                    if ((suma % 10 == 0) && (suma % 10 == verificador)) {
                        cedulaCorrecta = true;
                    } else if ((10 - (suma % 10)) == verificador) {
                        cedulaCorrecta = true;
                    } else {
                        cedulaCorrecta = false;
                    }
                } else {
                    cedulaCorrecta = false;
                }
            } else {
                cedulaCorrecta = false;
            }
        } catch (NumberFormatException nfe) {
            cedulaCorrecta = false;
        } catch (Exception err) {
            JOptionPane.showMessageDialog(null, "Una Excepción Ha Ocurrido En El Proceso de Validacion: "
                    + err.getMessage());
            cedulaCorrecta = false;
        }
        if (!cedulaCorrecta) {
            JOptionPane.showMessageDialog(null, "La Cédula Ingresada Es Incorrecta", "Mensaje",
                    JOptionPane.ERROR_MESSAGE);
            txt.setText("");
            txt.requestFocus();
        }
        return cedulaCorrecta;
    }

    /**
     * Recibimos parametros de la vista para validar los txt
     *
     * @param cad
     * @param evt
     */
    public void Mayusculas(String cad, KeyEvent evt) {
        char letra = evt.getKeyChar();
        if (Character.isLowerCase(letra)) {
            cad = ("" + letra).toUpperCase();
            letra = cad.charAt(0);
            evt.setKeyChar(letra);
        }
    }

    /**
     * Recibimos parametros de la vista para validar los txt
     * @param txt
     * @param evt 
     */
    public void EnterAJtexFiel(JTextField txt, KeyEvent evt) {
        char letra = evt.getKeyChar();
        if (letra == KeyEvent.VK_ENTER) {
            txt.requestFocus();
        }
    }
}
