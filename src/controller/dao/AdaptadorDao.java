/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller.dao;


import controller.lista.ListaSimple;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;

/**
 *
 * @author Mary Salazar Torres
 */
public class AdaptadorDao<T> implements InterfazDao<T>{
    
    private Conexion conexion;
    private Class<T> clazz;
    private String data_dir;

    public AdaptadorDao(Class<T> clazz) {
        this.conexion = new Conexion();
        this.clazz = clazz;
        data_dir = Conexion.REPOSITORIO + File.separatorChar+clazz.getSimpleName()+".xml";
    }
        
    
    @Override
    public void guardar(T dato) throws Exception {                
        ListaSimple<T> listado = this.listar();
        listado.insertarDato(dato);
        this.conexion.getxStream().toXML(listado, new FileOutputStream(data_dir));
    }

    @Override
    public ListaSimple<T> listar() throws Exception {
        ListaSimple<T> lista = new ListaSimple();
        try {
            lista = (ListaSimple<T>) conexion.getxStream().fromXML(new FileReader(data_dir));
            lista.setClase(clazz);
        } catch (Exception e) {
            System.out.println("error en listar "+e);
        }
        return lista;
    }

    @Override
    public void modificar(T dato, int pos) throws Exception {
        //buscamos por id
        // java reflect
        ListaSimple<T> lista = this.listar();
        lista.modificarDatoPos(pos, dato);
        this.conexion.getxStream().toXML(lista, new FileOutputStream(data_dir));
    }

    @Override
    public T obtener(Integer id) throws Exception {
//        return listar().consultarDatoPos(id);
        return null;
       
    }
    
}
