package controller.dao;

import controller.lista.ListaSimple;
import modelo.Cita;

/**
 * @author Anthony Luzuriaga
 */
public class CitaDao extends AdaptadorDao<Cita> {

    private Cita cita;

    public CitaDao() {
        super(Cita.class);
    }
    
    /**
     * Trae la cita, si es nula , crea una nueva cita.
     * @return la cita
     */
    public Cita getCita() {
        if (cita == null) {
            cita = new Cita();
        }
        return cita;
    }

    public void setCita(Cita cita) {
        this.cita = cita;
    }

    /**
     * Guarda la historia clinica
     *
     * @return true -> si se guardo correctamente , false -> si ah ocurrido un
     * error o no se pudo guardar.
     */
    public Boolean guardar() {
        try {
            this.cita.setId(listar().tamanio() + 1);
            guardar(this.cita);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Este metodo me permite modificar la la cita
     *
     * @param citita es el objeto a modificar
     * @param pos es la posicion del objeto a modificar
     * @return true si se modifico correctamente, false si no se pudo modificar.
     */
    public Boolean modi(Cita citita, int pos) {
        try {
            modificar(citita, pos);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Este metodo me permite modificar la cita
     *
     * @return true si se modifico correctamente, false si no se pudo modificar.
     */
    public Boolean modi2() {
        try {
            int pos = posicion(this.cita);
            modificar(this.cita, pos);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Me permte obtener la posicion del objeto enviado
     *
     * @param dato es el objeto del cual quiero la posicion.
     * @return la posicion de tipo entero.
     */
    private int posicion(Cita dato) {
        int pos = -1;
        try {
            ListaSimple<Cita> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Cita aux = lista.consultarDatoPos(i);
                if (dato.getId().equals(aux.getId())) {//busqueda atomica
                    pos = i;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return pos;
    }
}
