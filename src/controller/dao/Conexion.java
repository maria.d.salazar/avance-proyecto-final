
package controller.dao;

//import controlador.dao.*;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 *
 * @author Mary Salazar
 */
public class Conexion {
    public static final String REPOSITORIO = "datos";
    private XStream xStream;
    public Conexion() {
        xStream = new XStream(new DomDriver());
        xStream.setMode(XStream.NO_REFERENCES);
    }

    public XStream getxStream() {
        return xStream;
    }
    
}
