package controller.dao;

import controller.lista.ListaSimple;
import modelo.ConsultaMedica;

/**
 *
 * @author Angel Román
 */
public class ConsultaMedicaDao extends AdaptadorDao<ConsultaMedica> {

    private ConsultaMedica consulta;

    /**
     * Constructor de ConsultaMedicaDao
     */
    public ConsultaMedicaDao() {
        super(ConsultaMedica.class);
    }

    /**
     * Obtiene una consulta
     * @return consulta
     */
    public ConsultaMedica getConsulta() {
        if (consulta == null) {
            consulta = new ConsultaMedica();
        }
        return consulta;
    }

    /**
     * Envia una consulta
     * @param consulta 
     */
    public void setConsulta(ConsultaMedica consulta) {
        this.consulta = consulta;
    }

    /**
     * Metodo para guardar una consulta medica
     *
     * @return true si se guardo correctamente, false si no se pudo guardar
     */
    public Boolean guardar() {
        try {
            this.consulta.setId(listar().tamanio() + 1);
            guardar(this.consulta);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Metodo para buscar una consulta medica mediante id
     *
     * @param id es el id a buscar en la lista
     * @return el objeto consulta medica encontrado
     */
    public ConsultaMedica buscarConsulta(int id) {
        ConsultaMedica consulta = null;
        try {
            ListaSimple<ConsultaMedica> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                ConsultaMedica aux = lista.consultarDatoPos(i);
                if (id == aux.getId()) {//busqueda atomica
                    consulta = aux;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return consulta;
    }

    /**
     * Busca la posicion de una consulta medica
     *
     * @param dato objeto al cual le vamos a encontrar la posicion
     * @return la posicion del objeto
     */
    private int posicion(ConsultaMedica dato) {
        int pos = -1;
        try {
            ListaSimple<ConsultaMedica> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                ConsultaMedica aux = lista.consultarDatoPos(pos);
                if (dato.getId() == aux.getId()) {
                    pos = i;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return pos;
    }

    /**
     * Metodo para buscar objetos de la lista consulta medica que coincidan con
     * el id ingresado
     *
     * @param dato es el id a buscar en la lista
     * @return la lista simple que contiene todos los elementos encontrados
     */
    public ListaSimple<ConsultaMedica> buscar(Integer dato) {
        ListaSimple<ConsultaMedica> listado = new ListaSimple<>();
        try {
            ListaSimple<ConsultaMedica> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                ConsultaMedica aux = lista.consultarDatoPos(i);
                if (aux.getId().equals(dato)) {//busqueda atomica
                    listado.insertarDato(aux);
                }
            }
        } catch (Exception e) {
        }
        return listado;
    }
}
