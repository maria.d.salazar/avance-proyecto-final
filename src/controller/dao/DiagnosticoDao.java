package controller.dao;

import controller.lista.ListaSimple;
import modelo.Diagnostico;

/**
 *
 * @author Angel Román
 */
public class DiagnosticoDao extends AdaptadorDao<Diagnostico> {

    private Diagnostico diagnostico;
    
    /**
     * COnstructor de DiagnosticoDao
     */
    public DiagnosticoDao() {
        super(Diagnostico.class);
    }

    /**
     * Obtiene un diagnostico
     * @return diagnostico
     */
    public Diagnostico getDiagnostico() {
        if (diagnostico == null) {
            diagnostico = new Diagnostico();
        }
        return diagnostico;
    }

    /**
     * Envia un diagnostico
     * @param diagnostico 
     */
    public void setDiagnostico(Diagnostico diagnostico) {
        this.diagnostico = diagnostico;
    }

    /**
     * Metodo para guardar un diagnostico
     *
     * @return true si se guardo correctamente, false si no se pudo guardar
     */
    public Boolean guardar() {
        try {
            this.diagnostico.setId(listar().tamanio() + 1);
            guardar(this.diagnostico);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Metodo para buscar un diagnostico mediante id
     *
     * @param id es el id a buscar en la lista
     * @return el objeto diagnostico encontrado
     */
    public Diagnostico buscarDiagnostico(int id) {
        Diagnostico diagnostico = null;
        try {
            ListaSimple<Diagnostico> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Diagnostico aux = lista.consultarDatoPos(i);
                if (id == aux.getId()) {//busqueda atomica
                    diagnostico = aux;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return diagnostico;
    }

    /**
     * Busca la posicion de un diagnostico
     *
     * @param dato objeto al cual le vamos a encontrar la posicion
     * @return la posicion del objeto
     */
    private int posicion(Diagnostico dato) {
        int pos = -1;
        try {
            ListaSimple<Diagnostico> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Diagnostico aux = lista.consultarDatoPos(pos);
                if (dato.getId() == aux.getId()) {
                    pos = i;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return pos;
    }
}
