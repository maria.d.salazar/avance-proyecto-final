package controller.dao;

import controller.lista.ListaSimple;
import modelo.Especialidad;

/** 
 *@author Anthony Luzuriaga
 */
public class EspecialidadDao1 extends AdaptadorDao<Especialidad> {
    private Especialidad especialidad;
    private Especialidad especialidadAux;
    public static final int TIPO= 1;

    public EspecialidadDao1() {
        super(Especialidad.class);
    }

    public Especialidad getEspecialidad() {
        if (especialidad == null) {
            especialidad = new Especialidad();
        }
        return especialidad;
    }

    public void setEspecialidad(Especialidad especialidad) {
        this.especialidad = especialidad;
    }
    
    public Especialidad getEspecialidadAux() {
        if (especialidadAux == null) {
            especialidadAux = new Especialidad();
        }
        return especialidadAux;
    }

    public void setEspecialidadAux(Especialidad especialidadAux) {
        this.especialidadAux = especialidadAux;
    }
    
    public Boolean guardar() {
        try {
            this.especialidad.setId(listar().tamanio() + 1);
            guardar(this.especialidad);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

//    public Boolean modificar() {
//        try {
//            int pos = posicion(this.especialidad);
//            modificar(this.especialidad, pos);
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }
    
    public Boolean modi(Especialidad espAux, int pos) {
        try {
            modificar(espAux, pos);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    /**
     * Ordenar por nombre de especialidad
     */
    public ListaSimple<Especialidad> ordenar(String atributo, int tipo_ordenacion) {
        try {
            listar().setClase(Especialidad.class);
            return listar().ordenar(atributo, tipo_ordenacion);
        } catch (Exception e) {
            System.out.println("error "+e);
            return new ListaSimple<>();
        }
    }

    private int posicion(Especialidad dato) {
        int pos = -1;
        try {
            ListaSimple<Especialidad> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Especialidad aux = lista.consultarDatoPos(pos);
                if (dato.getId() == aux.getId()) {//busqueda atomica
                    pos = i;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return pos;
    }
    
    public Especialidad buscariD(Integer nro_inv) {
        Especialidad especialidd = null;
        try {
            ListaSimple<Especialidad> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Especialidad aux = lista.consultarDatoPos(i);
                if (nro_inv.equals(aux.getId())) {//busqueda atomica
                    especialidd = aux;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return especialidd;
    }
    
    public Especialidad buscarnroNombre(String nro_inv) {        
        Especialidad especialidd = null;
        try {
            ListaSimple<Especialidad> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Especialidad aux = lista.consultarDatoPos(i);
                if (nro_inv.equalsIgnoreCase(aux.getTipo())) {//busqueda atomica
                    especialidd= aux;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return especialidd;
    }
    
    public ListaSimple<Especialidad> buscar(String dato, int tipo) {        
        ListaSimple<Especialidad> listado = new ListaSimple<>();
        try {
            ListaSimple<Especialidad> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Especialidad aux = lista.consultarDatoPos(i);
                switch(tipo) {
                    case 1: 
                        if (aux.getTipo().toUpperCase().contains(dato.toUpperCase())) {//busqueda atomica
                            listado.insertarDato(aux);
                        }
                        break;
                }
            }
        } catch (Exception e) {
        }
        
        return listado;
    }
}
