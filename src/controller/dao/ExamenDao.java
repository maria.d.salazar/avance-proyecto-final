package controller.dao;

import controller.lista.ListaSimple;
import modelo.Examen;

/**
 *
 * @author Angel Román
 */
public class ExamenDao extends AdaptadorDao<Examen> {

    private Examen examen;
    
    /**
     * Contructor de ExamenDao
     */
    public ExamenDao() {
        super(Examen.class);
    }

    /**
     * Obtiene un examen
     * @return examen
     */
    public Examen getExamen() {
        if (examen == null) {
            examen = new Examen();
        }
        return examen;
    }

    /**
     * Envia un examen
     * @param examen 
     */
    public void setExamen(Examen examen) {
        this.examen = examen;
    }

    /**
     * Metodo para guardar una consulta medica
     *
     * @return true si se guardo correctamente, false si no se pudo guardar
     */
    public Boolean guardar() {
        try {
            this.examen.setId(listar().tamanio() + 1);
            guardar(this.examen);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Metodo para buscar un examen mediante id
     *
     * @param id es el id a buscar en la lista
     * @return el objeto examen encontrado
     */
    public Examen buscarExamen(int id) {
        Examen examen = null;
        try {
            ListaSimple<Examen> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Examen aux = lista.consultarDatoPos(i);
                if (id == aux.getId()) {//busqueda atomica
                    examen = aux;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return examen;
    }

    /**
     * Busca la posicion de un examen
     *
     * @param dato objeto al cual le vamos a encontrar la posicion
     * @return la posicion del objeto
     */
    private int posicion(Examen dato) {
        int pos = -1;
        try {
            ListaSimple<Examen> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Examen aux = lista.consultarDatoPos(pos);
                if (dato.getId() == aux.getId()) {
                    pos = i;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return pos;
    }

    /**
     * Metodo para buscar objetos de la lista examen los cuales coincidan con el
     * id ingresado
     *
     * @param dato es el id a buscar en la lista
     * @return la lista simple que contiene todos los elementos encontrados
     */
    public ListaSimple<Examen> buscar(Integer dato) {
        ListaSimple<Examen> listado = new ListaSimple<>();
        try {
            ListaSimple<Examen> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Examen aux = lista.consultarDatoPos(i);
                if (aux.getNroConsulta().equals(dato)) {//busqueda atomica
                    listado.insertarDato(aux);
                }
            }
        } catch (Exception e) {
        }
        return listado;
    }
}
