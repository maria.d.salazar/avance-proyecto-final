package controller.dao;

import controller.lista.ListaSimple;
import modelo.HistorialPaciente;

public class HistorialPacienteDao extends AdaptadorDao<HistorialPaciente> {
    private HistorialPaciente historialP;
    private HistorialPaciente historialPAUX;
    public static final int NROHISTORIACLINICA = 1;
    public static final int CEDULA = 2;
    
    public HistorialPacienteDao() {
        super(HistorialPaciente.class);
    }

    public HistorialPaciente getHistorialP() {
        if (historialP == null) {
            historialP = new HistorialPaciente();
        }
        return historialP;
    }

    public HistorialPaciente getHistorialPAUX() {
        if (historialPAUX == null) {
            historialPAUX = new HistorialPaciente();
        }
        return historialPAUX;
    }

    public void setHistorialPAUX(HistorialPaciente historialPAUX) {
        this.historialPAUX = historialPAUX;
    }
    
    public void setHistorialP(HistorialPaciente historialP) {
        this.historialP = historialP;
    }

    /**
     * Guarda la historia clinica 
     * @return true si se guardo correctamente , false si ah ocurrido un error o no se pudo guardar.
     */
    public Boolean guardar() {
        try {
            this.historialP.setId(listar().tamanio() + 1);
            guardar(this.historialP);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    /**
     * Este metodo me permite modificar la historia del paciente.
     * @param historialPauxiliar es el objeto a modificar
     * @param pos es la posicion del objeto a modificar
     * @return true si se modifico correctamente, false si no se pudo modificar.
     */
    public Boolean modi(HistorialPaciente historialPauxiliar, int pos) {
//    public Boolean modificar() {
        try {
//            int pos = posicion(this.historialP);
            modificar(historialPauxiliar, pos);
//            modificar(this.historialP, pos);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Ordena segun el atributo y el tipo de ordenacion que se le indique
     * @param atributo es el de acuerdo a la cual se ordenara
     * @param tipo_ordenacion si es ascendete (1), si es descendente (2);
     * @return 
     */
    public ListaSimple<HistorialPaciente> ordenar(String atributo, int tipo_ordenacion) {
        try {
            listar().setClase(HistorialPaciente.class);
            return listar().ordenar(atributo, tipo_ordenacion);
        } catch (Exception e) {
            System.out.println("error "+e);
            return new ListaSimple<>();
        }
    }
    
    /**
     * Me permte obtener la posicion del objeto enviado
     * @param dato es el objeto del cual quiero la posicion.
     * @return la posicion
     */
    public int posicion(HistorialPaciente dato) {
        int pos = -1;
        try {
            ListaSimple<HistorialPaciente> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                HistorialPaciente aux = lista.consultarDatoPos(i);
//                if (dato.getId() == aux.getId()) {//busqueda atomica
                if (dato.getNroDeHistoriaClinica()== aux.getNroDeHistoriaClinica()){
                    pos = i;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return pos;
    }
    
    /**
     * Busca por el nro de historial del paciente
     * @param nro_inv es el nro de historial que quiero buscar
     * @return el objeto donde coincida la busqueda
     */
    public HistorialPaciente buscarnroHistorial(int nro_inv) {        
        HistorialPaciente historialPaciente = null;
        try {
            ListaSimple<HistorialPaciente> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                HistorialPaciente aux = lista.consultarDatoPos(i);
                if (nro_inv == (aux.getNroDeHistoriaClinica())) {//busqueda atomica
                    historialPaciente = aux;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return historialPaciente;
    }
    
    /**
     * Busca por el ud del paciente
     * @param id es el id que quiero buscar
     * @return el objeto donde coincida la busqueda
     */
     public HistorialPaciente buscarID(int id) {        
        HistorialPaciente historialPaciente = null;
        try {
            ListaSimple<HistorialPaciente> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                HistorialPaciente aux = lista.consultarDatoPos(i);
                if (id == (aux.getId())) {//busqueda atomica
                    historialPaciente = aux;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return historialPaciente;
    }
    
     /**
      * Es un buscar general 
      * @param dato es el atributo que quiero buscar
      * @param tipo si es ascendete o descendente.
      * @return  la lista simple del historial paciente.
      */
    public ListaSimple<HistorialPaciente> buscar(String dato, int tipo) {        
        ListaSimple<HistorialPaciente> listado = new ListaSimple<>();
        try {
            ListaSimple<HistorialPaciente> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                HistorialPaciente aux = lista.consultarDatoPos(i);
                switch(tipo) {
                    case 1: 
                        if (String.valueOf(aux.getNroDeHistoriaClinica()).contains(dato)) {//busqueda atomica
                            listado.insertarDato(aux);
                        }
                        break;
                    case 2: 
                        if (aux.getPaciente().getCedula().toUpperCase().equalsIgnoreCase(dato.toUpperCase())) {//busqueda atomica
                            listado.insertarDato(aux);
                        }
                        break;
                    case 3: 
                        String np = aux.getPaciente().getNombre()+""+aux.getPaciente().getApellido();
                        if (np.toUpperCase().equalsIgnoreCase(dato.toUpperCase())){//busqueda atomica
                            listado.insertarDato(aux);
                        }
                        break;
                }
            }
        } catch (Exception e) {
        }
        
        return listado;
    }
}
