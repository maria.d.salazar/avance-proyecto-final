/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller.dao;

import controller.dao.*;
import controller.lista.ListaSimple;

/**
 *
 * @author sebas
 */
public interface InterfazDao <T> {
    public void guardar(T dato) throws Exception;
    public void modificar(T dato, int pos) throws Exception;
    public ListaSimple<T> listar() throws Exception;
    public T obtener(Integer id) throws Exception;
}
