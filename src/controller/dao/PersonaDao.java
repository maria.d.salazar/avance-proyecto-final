/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.dao;

import controller.lista.ListaSimple;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import modelo.Persona;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Mary Salazar
 */
public class PersonaDao extends AdaptadorDao<Persona> {

    private Persona persona;
    private Persona personaAux;
    private ListaSimple<Persona> listado;
    public static final int ESPECIALIDAD = 1;
    public static final int NOMBRE = 2;

    /**
     * Constructor de CuentaDao
     */
    public PersonaDao() {
        super(Persona.class);

    }

    /**
     * Recibe un objeto de tipo persona
     *
     * @return persona
     */
    public Persona getPersona() {
        if (persona == null) {
            persona = new Persona();
        }
        return persona;
    }

    /**
     * Envia un objeto de tipo persona
     *
     * @param persona
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    /**
     * Recibe un objeto de tipo persona
     *
     * @return persona aux
     */
    public Persona getPersonaAux() {
        if (personaAux == null) {
            personaAux = new Persona();
        }
        return personaAux;
    }

    /**
     * Envia un objeto de tipo persona
     *
     * @param personaAux
     */
    public void setPersonaAux(Persona personaAux) {
        this.personaAux = personaAux;
    }

    /**
     * Metodo para verificar si se guardo o no una persona
     *
     * @return Boolean
     */
    public Boolean guardar() {
        try {
            this.persona.setId(listar().tamanio() + 1);
            guardar(this.persona);
            return true;
        } catch (Exception e) {
            System.out.println("error " + e);
            return false;
        }
    }

    /**
     * Nos permite actualizar los datos de persona
     *
     * @param persoAux
     * @param pos
     * @return Boolean
     */
    public Boolean modi(Persona persoAux, int pos) {
        try {
            modificar(persoAux, pos);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Devuelve la posicion en la que se encuentra una persona
     *
     * @param persona
     * @return pos
     */
    public int posicion(Persona persona) {
        int pos = 0;
        try {
            ListaSimple<Persona> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Persona aux = lista.consultarDatoPos(pos);
                if (persona.getId().intValue() == aux.getId().intValue()) {
                    pos = i;
                    break;
                } else {
                    pos++;
                }
            }
        } catch (Exception e) {
        }
        return pos;
    }

    /**
     * Obtiene los datos de la persona de acuerdo a la cedula
     *
     * @param ced
     * @return person
     */
    public Persona buscarCedula(String ced) {
        Persona person = null;
        try {
            ListaSimple<Persona> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Persona aux = lista.consultarDatoPos(i);
                if (ced.equals(aux.getCedula())) {//busqueda atomica
                    person = aux;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return person;
    }

    /**
     * Este metodo permite verificar el rol con el que ha ingresado la persona
     * en el xml
     *
     * @param usuario
     * @param password
     * @return rol
     */
    public String Autentificar(String usuario, String password) {
        String rol = "";
        try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documentoXml = builder.parse("datos\\Persona.xml");
            documentoXml.getDocumentElement().normalize();

            String consultaUsuario = "//usuario";
            String consultaClave = "//clave";
            String consultaRol = "//rol";

            XPath xpath = XPathFactory.newInstance().newXPath();

            NodeList nodo = (NodeList) xpath.evaluate(consultaUsuario, documentoXml, XPathConstants.NODESET);
            NodeList nodo1 = (NodeList) xpath.evaluate(consultaClave, documentoXml, XPathConstants.NODESET);
            NodeList nodo2 = (NodeList) xpath.evaluate(consultaRol, documentoXml, XPathConstants.NODESET);

            for (int i = 0; i < nodo.getLength(); i++) {
                if (nodo.item(0).getTextContent().equals(usuario) && nodo1.item(0).getTextContent().equals(password) && nodo2.item(0).getTextContent().equals("ADMINISTRADOR")) {
                    rol = "Principal";
                } else if (nodo.item(i).getTextContent().equals(usuario) && nodo1.item(i).getTextContent().equals(password) && nodo2.item(i).getTextContent().equals("MEDICO")) {
                    rol = "Doctor";
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException ex) {
            System.out.println("Ha ocurrido un error al leer el archivo" + ex);
        }
        System.out.println("" + rol);
        return rol;
    }

    /**
     * Devuelve un listado de todas las personas registradas como ADMINISTRADOR
     *
     * @return aux
     */
    public ListaSimple<Persona> getListado() {
        try {
            listado = listar();
            ListaSimple<Persona> aux = new ListaSimple<>();
            for (int i = 0; i < listado.tamanio(); i++) {
                if (listado.consultarDatoPos(i).getRol().equals("ADMINISTRADOR")) {
                    aux.insertarDato(listado.consultarDatoPos(i));
                }
            }
            return aux;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Devuelve un listado de todas las personas registradas como MEDICO
     *
     * @return aux
     */
    public ListaSimple<Persona> getListado2() {
        try {
            listado = listar();
            ListaSimple<Persona> aux = new ListaSimple<>();
            for (int i = 0; i < listado.tamanio(); i++) {
                if (listado.consultarDatoPos(i).getRol().equals("MEDICO")) {
                    aux.insertarDato(listado.consultarDatoPos(i));
                }
            }
            return aux;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Devuelve un listado de todas las personas registradas como PACIENTE
     *
     * @return aux
     */
    public ListaSimple<Persona> getListado3() {
        try {
            listado = listar();
            ListaSimple<Persona> aux = new ListaSimple<>();
            for (int i = 0; i < listado.tamanio(); i++) {
                if (listado.consultarDatoPos(i).getRol().equals("PACIENTE")) {
                    aux.insertarDato(listado.consultarDatoPos(i));
                }
            }
            return aux;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Nos permite enviar el listado de personas
     *
     * @param listado
     */
    public void setListado(ListaSimple<Persona> listado) {
        this.listado = listado;
    }

    /*
     * Metodo que busca de acuerdo a la especialidad o al nombre  del medico
     * @param dato -> dato de la vista que se compara
     * @param tipo -> referencia al tipo de dato que buscara.
     * @return lista de persona
     */
    public ListaSimple<Persona> buscar(String dato, int tipo) {
        ListaSimple<Persona> listado = new ListaSimple<>();
        try {
            ListaSimple<Persona> lista = getListado2();
            for (int i = 0; i < lista.tamanio(); i++) {
                Persona aux = lista.consultarDatoPos(i);
                switch (tipo) {
                    case 1:
                        if (String.valueOf(aux.getEspecialidad().getTipo()).toUpperCase().equalsIgnoreCase(dato)) {//busqueda atomica
                            listado.insertarDato(aux);
                            System.out.println("si");
                        }
                        System.out.println("no");
                        break;
                    case 2:
                        if (aux.getNombre().toUpperCase().equalsIgnoreCase(dato.toUpperCase())) {//busqueda atomica
                            listado.insertarDato(aux);
                        }
                        break;
                }
            }
        } catch (Exception e) {
        }
        return listado;
    }
}
