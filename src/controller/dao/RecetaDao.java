package controller.dao;

import modelo.Receta;

/**
 *
 * @author Anthony Luzuraiga
 */
public class RecetaDao extends AdaptadorDao<Receta> {

    private Receta receta;

    public RecetaDao() {
        super(Receta.class);
    }

    /**
     * Este metodo me devuelve la receta, y si no hay receta crea una nueva receta
     * @return la receta
     */
    public Receta getReceta() {
        if (receta == null) {
            receta = new Receta();
        }
        return receta;
    }
    
    /**
     * Este metodo me setea la receta que le indique 
     * @param receta es la receta por la que se cambiara.
     */
    public void setReceta(Receta receta) {
        this.receta = receta;
    }

    /**
     * Este metodo me permite guardar el objeto receta
     * @return true-> si se ah guardado correctamente, false -> si no se ah guardado o ah ocurrido un error.
     */
    public Boolean guardar() {
        try {
            this.receta.setId(listar().tamanio() + 1);
            guardar(this.receta);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Este metodo me permite modificar una receta dado el objeto y su posicion.
     * @param recetita -> Es el objeto de la receta que se va modificar
     * @param pos -> Es la posicion donde se encuentra es el objeto que quiero modificar.
     * @return true-> si se ah modificado con exito; false -> si no se ah podido modificar
     */
    public Boolean modi(Receta recetita, int pos) {
//    public Boolean modificar() {
        try {
//            int pos = posicion(this.historialP);
            modificar(recetita, pos);
//            modificar(this.historialP, pos);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
