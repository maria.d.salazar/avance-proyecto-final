/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.lista;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 *
 * @author Mary Salazar
 */
public class ListaSimple<T> {

    public NodoDato cabecera;
    private Class<T> clase;
    public static final int ASCENDENTE = 1;
    public static final int DESCENDENTE = 2;

    /**
     * Constructor vacio
     */
    public ListaSimple() {
        this.cabecera = null;
    }

    /**
     * Constructor generico
     *
     * @param clase
     */
    public void setClase(Class<T> clase) {
        this.clase = clase;
    }

    /**
     * Comprobar si la lista esta vacia
     *
     * @return null
     */
    public boolean estaVacio() {
        return (this.cabecera == null);
    }

    /**
     * Imprime los datos de la lista
     */
    public void imprimir() {
        if (!estaVacio()) {
            NodoDato tmp = cabecera;//
            System.out.println("------");
            while (tmp != null) {
                System.out.print(tmp.getDato() + "\t");
                tmp = tmp.getSiguiente();
            }
            System.out.println("------");
        }
    }

    /**
     * Metodo para insertar primer dato de la lista
     *
     * @param dato
     */
    private void insertar(T dato) {

        NodoDato tmp = new NodoDato(dato, cabecera);
        cabecera = tmp;
    }

    /**
     * Agrega mas datos a la lista en caso de que esta ya este llena
     *
     * @param dato
     * @return Boolean
     */
    public boolean insertarDato(T dato) {
        try {
            inserTarFinal(dato);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Extrae el primer elemento de la lista
     *
     * @return dato
     */
    public T extraer() {
        T dato = null;
        if (!estaVacio()) {
            dato = (T) cabecera.getDato();
            cabecera = cabecera.getSiguiente();
        }
        return dato;
    }

    /**
     * Nos devuelve la posicion de un objeto de la lista
     *
     * @param pos
     * @return dato
     */
    public T consultarDatoPos(int pos) {
        T dato = null;
        if (!estaVacio() && (pos <= tamanio() - 1)) {
            NodoDato tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                dato = (T) tmp.getDato();
            }
        }
        return dato;
    }

    /**
     * Nos permite modificar un dato a partir de su posicion
     *
     * @param pos
     * @param data
     * @return Boolean
     */
    public boolean modificarDatoPos(int pos, T data) {

        if (!estaVacio() && (pos <= tamanio() - 1)) {
            NodoDato tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                tmp.setDato(data);
                return true;
            }
        }
        return false;
    }

    /**
     * Nos da el tamanio de la lista
     *
     * @return tamanio
     */
    public int tamanio() {
        int tamanio = 0;
        if (!estaVacio()) {
            NodoDato tmp = cabecera;
            while (tmp != null) {
                tamanio++;
                tmp = tmp.getSiguiente();
            }
        }
        return tamanio;
    }

    /**
     * Nos permite insertar o reemplazar un dato en una posicion especifica
     *
     * @param dato
     * @param pos
     */
    public void insertar(T dato, int pos) {
        if (estaVacio() || pos < 0) {
            insertar(dato);
        } else {
            NodoDato iterador = cabecera;
            for (int i = 0; i < pos; i++) {
                if (iterador.getSiguiente() == null) {
                    break;
                }
                iterador = iterador.getSiguiente();
            }
            NodoDato tmp = new NodoDato(dato, iterador.getSiguiente());
            iterador.setSiguiente(tmp);

        }
    }

    /**
     * Inserta un dato al final de la lista
     *
     * @param dato
     */
    private void inserTarFinal(T dato) {
        insertar(dato, tamanio() - 1);
    }

    /**
     * Permite ordenar las listas de forma simple
     *
     * @param field El nombre del atributo d ela clase con el cual quiero
     * ordenar
     * @param tipo_ordenacion 1:ASCENDENTE 2:DESCENDENTE, otro valor no ordenara
     * @return ListaSimple ordenada
     */
    public ListaSimple<T> ordenar(String field, int tipo_ordenacion) {
        try {
            ListaSimple<T> lista = this;
            int intercambio = 0;
            for (int i = 0; i < lista.tamanio() - 1; i++) {
                int k = i;
                //Libro t = lista.consultarDatoPos(i);//datos[i];            
                T t = this.consultarDatoPos(i);
                for (int j = i + 1; j < lista.tamanio(); j++) {
                    String valor = getValueField(getFlied(field), consultarDatoPos(j)).toString();
                    if (tipo_ordenacion == ListaSimple.ASCENDENTE) {
                        if (valor.toLowerCase().compareTo(getValueField(getFlied(field), t).toString().toLowerCase()) < 0) {
                            t = lista.consultarDatoPos(j);
                            k = j;
                            intercambio++;
                        }
                    } else if (tipo_ordenacion == ListaSimple.DESCENDENTE) {
                        if (valor.toLowerCase().compareTo(getValueField(getFlied(field), t).toString().toLowerCase()) > 0) {
                            t = lista.consultarDatoPos(j);
                            k = j;
                            intercambio++;
                        }
                    }
                }
                lista.modificarDatoPos(k, lista.consultarDatoPos(i));
                lista.modificarDatoPos(i, t);
            }
            return lista;
        } catch (Exception e) {
            System.out.println("error en lista ordenar " + e);
            e.printStackTrace();
            return new ListaSimple<>();
        }
    }

    /**
     *
     * @param atribute_name
     * @return field
     */
    private Field getFlied(String atribute_name) {
        Field field = null;

        for (Field aux : this.clase.getDeclaredFields()) {
            if (atribute_name.equalsIgnoreCase(aux.getName())) {
                field = aux;
                break;
            }
        }
        if (field == null) {
            for (Field aux : this.clase.getSuperclass().getDeclaredFields()) {
                if (atribute_name.equalsIgnoreCase(aux.getName())) {
                    field = aux;
                    break;
                }
            }
        }
        return field;
    }

    /**
     *
     * @param field
     * @param dato
     * @return
     * @throws Exception
     */
    private Object getValueField(Field field, T dato) throws Exception {
        Method metodo = null;
        for (Method aux : this.clase.getDeclaredMethods()) {
            if (aux.getName().toLowerCase().equalsIgnoreCase("get" + field.getName().toLowerCase())) {
                metodo = aux;
            }
        }
        if (metodo == null) {
            for (Method aux : this.clase.getSuperclass().getDeclaredMethods()) {
                if (aux.getName().toLowerCase().equalsIgnoreCase("get" + field.getName().toLowerCase())) {
                    metodo = aux;
                }
            }
        }
        return metodo.invoke(dato);
    }

}
