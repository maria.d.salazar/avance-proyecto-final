/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.lista;

/**
 *
 * @author Usuario
 */
public class NodoDato <T> {
    private T dato;
    private NodoDato siguiente;

    /**
     * Constructor de NodoDato
     * @param dato
     * @param nodo 
     */
    public NodoDato(T dato, NodoDato nodo) {
        this.dato = dato;
        this.siguiente = nodo;
    }

    /**
     * Constructor vacio para nuevo dato
     */
    public NodoDato() {
        this.dato = null;
        this.siguiente = null;
    }
       
    /**
     * Obtener un dato
     * @return dato
     */
    public T getDato() {
        return dato;
    }

    /**
     * Enviar dato de tipo T o generico
     * @param dato 
     */
    public void setDato(T dato) {
        this.dato = dato;
    }
    
    /**
     * Obtener siguiente dato
     * @return  siguiente
     */
    public NodoDato getSiguiente() {
        return siguiente;
    }
    
    /**
     * Enviar siguiente dato
     * @param siguiente 
     */
    public void setSiguiente(NodoDato siguiente) {
        this.siguiente = siguiente;
    }
}
