
package modelo;

/**
 * @author Marco, Anthony luzuriaga
 */
public class Cita {
    private Integer id;
    private Persona persona;
    private Persona medico;
//    private Especialidad lista;
    private String fecha;
    private String hora;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

//    /**
//     * 
//     * @return la especialidad
//     */
//    public Especialidad getLista() {
//        return lista;
//    }
//    
//    public void setLista(Especialidad lista) {
//        this.lista = lista;
//    }

    /**
     * @return the persona
     */
    public Persona getPersona() {
        if(persona == null){
            persona = new Persona();
        }
        return persona;
    }

    /**
     * @param persona the persona to set
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    /**
     * @return the hora
     */
    public String getHora() {
        return hora;
    }

    /**
     * @param hora the hora to set
     */
    public void setHora(String hora) {
        this.hora = hora;
    }

    /**
     * @return the medico
     */
    public Persona getMedico() {
        if(medico == null){
            medico = new Persona();
        }
        return medico;
    }

    /**
     * @param medico the medico to set
     */
    public void setMedico(Persona medico) {
        this.medico = medico;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
}
