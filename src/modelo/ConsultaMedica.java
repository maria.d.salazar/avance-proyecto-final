package modelo;

/**
 *
 * @author Angel Román
 */
public class ConsultaMedica {

    private Integer id;
    private String fecha;
    private String cedulaMedico;
    private Double temperatura;
    private String presionArterial;
    private String saturacionOxigeno;
    private String motivoConsulta;
    private Integer nroHistoria;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the cedulaMedico
     */
    public String getCedulaMedico() {
        return cedulaMedico;
    }

    /**
     * @param cedulaMedico the cedulaMedico to set
     */
    public void setCedulaMedico(String cedulaMedico) {
        this.cedulaMedico = cedulaMedico;
    }

    /**
     * @return the temperatura
     */
    public Double getTemperatura() {
        return temperatura;
    }

    /**
     * @param temperatura the temperatura to set
     */
    public void setTemperatura(Double temperatura) {
        this.temperatura = temperatura;
    }

    /**
     * @return the presionArterial
     */
    public String getPresionArterial() {
        return presionArterial;
    }

    /**
     * @param presionArterial the presionArterial to set
     */
    public void setPresionArterial(String presionArterial) {
        this.presionArterial = presionArterial;
    }

    /**
     * @return the saturacionOxigeno
     */
    public String getSaturacionOxigeno() {
        return saturacionOxigeno;
    }

    /**
     * @param saturacionOxigeno the saturacionOxigeno to set
     */
    public void setSaturacionOxigeno(String saturacionOxigeno) {
        this.saturacionOxigeno = saturacionOxigeno;
    }

    /**
     * @return the motivoConsulta
     */
    public String getMotivoConsulta() {
        return motivoConsulta;
    }

    /**
     * @param motivoConsulta the motivoConsulta to set
     */
    public void setMotivoConsulta(String motivoConsulta) {
        this.motivoConsulta = motivoConsulta;
    }

    /**
     * @return the nroHistoria
     */
    public Integer getNroHistoria() {
        return nroHistoria;
    }

    /**
     * @param nroHistoria the nroHistoria to set
     */
    public void setNroHistoria(Integer nroHistoria) {
        this.nroHistoria = nroHistoria;
    }
}
