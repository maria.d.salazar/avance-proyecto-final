package modelo;

/**
 *
 * @author Angel Román
 */
public class Diagnostico {

    private Integer id;
    private Integer nroConsulta;
    private String descripcion;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nroConsulta
     */
    public Integer getNroConsulta() {
        return nroConsulta;
    }

    /**
     * @param nroConsulta the nroConsulta to set
     */
    public void setNroConsulta(Integer nroConsulta) {
        this.nroConsulta = nroConsulta;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
