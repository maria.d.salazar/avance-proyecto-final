
package modelo;

import java.io.Serializable;
/**
 * @author Anthony Luzuriaga
 */

public class HistorialPaciente implements Serializable {
    private Integer id;
    private Integer nroDeHistoriaClinica;
    private Persona paciente;
    private String habito;
    private String grupoSanguineo;
    private String enfermedad;
    private String enfermedadHereditaria;
    private String alergias;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNroDeHistoriaClinica() {
        return nroDeHistoriaClinica;
    }

    public void setNroDeHistoriaClinica(Integer nroDeHistoriaClinica) {
        this.nroDeHistoriaClinica = nroDeHistoriaClinica;
    }
    
    public Persona getPaciente(){
        if(paciente == null){
            paciente = new Persona();
        }
        return paciente;
    }
    
    public void setPaciente(Persona p){
        this.paciente = p;
    }
    
    public String getHabito() {
        return habito;
    }

    public void setHabito(String habito) {
        this.habito = habito;
    }

    public String getGrupoSanguineo() {
        return grupoSanguineo;
    }

    public void setGrupoSanguineo(String grupoSanguineo) {
        this.grupoSanguineo = grupoSanguineo;
    }

    public String getEnfermedad() {
        return enfermedad;
    }

    public void setEnfermedad(String enfermedad) {
        this.enfermedad = enfermedad;
    }

    public String getEnfermedadHereditaria() {
        return enfermedadHereditaria;
    }

    public void setEnfermedadHereditaria(String enfermedadHereditaria) {
        this.enfermedadHereditaria = enfermedadHereditaria;
    }

    public String getAlergias() {
        return alergias;
    }

    public void setAlergias(String alergias) {
        this.alergias = alergias;
    }     
}
