package vista;

import controller.Validaciones;
import controller.dao.ConsultaMedicaDao;
import controller.dao.HistorialPacienteDao;
import controller.dao.PersonaDao;
import controller.lista.ListaSimple;
import java.awt.HeadlessException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import modelo.HistorialPaciente;
import vistas.modelo.ModeloTablaConsultaMedica;

/**
 *
 * @author Angel Román
 */
public class Frm_ConsultaMedica extends javax.swing.JFrame {

    private ConsultaMedicaDao consultaDao = new ConsultaMedicaDao();
    private HistorialPacienteDao historialDao = new HistorialPacienteDao();
    private PersonaDao personaDao = new PersonaDao();
    private ModeloTablaConsultaMedica modelo = new ModeloTablaConsultaMedica();
    Validaciones validar = new Validaciones();
    Integer aux;

    public Frm_ConsultaMedica() {
        initComponents();
        this.setLocationRelativeTo(null);
        actualizarCombo();
        limpiar();
        cargarTabla();
        btnactualizarCM2.setVisible(false);
    }

    /**
     * Metodo para cargar la tabla consulta medica
     */
    private void cargarTabla() {
        try {
            modelo.setLista(consultaDao.listar());
            tblCM.setModel(modelo);
            tblCM.updateUI();
        } catch (Exception ex) {
            Logger.getLogger(Frm_ConsultaMedica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para actualizar combobox con la informacion de historial paciente
     * y persona
     */
    public void actualizarCombo() {
        try {
            DefaultComboBoxModel combo = new DefaultComboBoxModel();
            String[] nroHistoria = new String[historialDao.listar().tamanio()];
            for (int i = 0; i < historialDao.listar().tamanio(); i++) {
                nroHistoria[i] = historialDao.listar().consultarDatoPos(i).getNroDeHistoriaClinica().toString();
                combo.addElement(nroHistoria[i]);
            }
            cbxhistorialCM.setModel(combo);
        } catch (Exception ex) {
            Logger.getLogger(Frm_Receta.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            DefaultComboBoxModel combo = new DefaultComboBoxModel();
            String[] cedulaMedico = new String[personaDao.listar().tamanio()];
            for (int i = 0; i < personaDao.listar().tamanio(); i++) {
                cedulaMedico[i] = personaDao.listar().consultarDatoPos(i).getCedula();
                if ("MEDICO".equals(personaDao.listar().consultarDatoPos(i).getRol())) {
                    combo.addElement(cedulaMedico[i]);
                }
            }
            cbxcedulaCM.setModel(combo);
        } catch (Exception e) {
        }
    }

    /**
     * Metodo para limpiar cuadros de texto y reestablecer combobox
     */
    private void limpiar() {
        txtmotivoCM.setText("");
        txttemperaturaCM.setText("");
        txtpresionCM.setText("");
        txtsaturacionCM.setText("");
        cbxcedulaCM.setSelectedIndex(-1);
        cbxhistorialCM.setSelectedIndex(-1);
    }

    /**
     * Metodo para validar cuadros de texto
     *
     * @return true si los cuadros de texto no estan vacios, false si estan
     * vacios
     */
    private boolean validarDatos() {
        return !("".equals(txtmotivoCM.getText()) || "".equals(txtpresionCM.getText()) || "".equals(txtsaturacionCM.getText()));
    }

    /**
     * Metodo para guardar la consulta medica con los datos ingresados
     */
    private void guardar() {
        try {
            consultaDao.getConsulta().setFecha(fechaActual());
            consultaDao.getConsulta().setCedulaMedico(cbxcedulaCM.getSelectedItem().toString());
            consultaDao.getConsulta().setNroHistoria(Integer.parseInt(cbxhistorialCM.getSelectedItem().toString()));
            consultaDao.getConsulta().setTemperatura(Double.parseDouble(txttemperaturaCM.getText()));
            consultaDao.getConsulta().setPresionArterial(txtpresionCM.getText());
            consultaDao.getConsulta().setSaturacionOxigeno(txtsaturacionCM.getText());
            consultaDao.getConsulta().setMotivoConsulta(txtmotivoCM.getText());
            if (validarDatos() && consultaDao.guardar()) {
                JOptionPane.showMessageDialog(this, "Se ha registrado correctamente");
            } else {
                JOptionPane.showMessageDialog(this, "No se pudo registrar");
            }
        } catch (HeadlessException | NumberFormatException | NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Llene correctamente la informacion de todos los campos");
        }
    }

    /**
     * Metodo para modificar una consulta medica seleccionada en la tabla
     */
    private void modificar() {
        try {
            if (validarDatos()) {
                int id = (Integer) tblCM.getValueAt(tblCM.getSelectedRow(), 0);
                consultaDao.setConsulta(consultaDao.buscarConsulta(id));
                consultaDao.getConsulta().setTemperatura(Double.parseDouble(txttemperaturaCM.getText()));
                consultaDao.getConsulta().setPresionArterial(txtpresionCM.getText());
                consultaDao.getConsulta().setSaturacionOxigeno(txtsaturacionCM.getText());
                consultaDao.getConsulta().setMotivoConsulta(txtmotivoCM.getText());
                try {
                    consultaDao.modificar(consultaDao.getConsulta(), id - 1);
                    JOptionPane.showMessageDialog(this, "Se ha modificado correctamente");
                    deshabilitarModificacion();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, "No se pudo modificar");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Llene correctamente la informacion de todos los campos");
            }
        } catch (HeadlessException | NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Llene correctamente la informacion de todos los campos");
        }

    }

    /**
     * Metodo para obtener la fecha actual del sistema
     *
     * @return la fecha obtenida
     */
    public String fechaActual() {
        LocalDateTime fechaLocal = LocalDateTime.now();
        int dia = fechaLocal.getDayOfMonth();
        int mes = fechaLocal.getMonthValue();
        int anio = fechaLocal.getYear();
        String fecha = dia + "/" + mes + "/" + anio;
        return fecha;
    }

    /**
     * Metodo para llenar los cuadros de texto, combobox, habilitar los botones
     * de vista y modificacion de datos de una consulta medica seleccionada en
     * la lista
     */
    private void habilitarModificacion() {
        try {
            cbxhistorialCM.setSelectedItem(tblCM.getValueAt(tblCM.getSelectedRow(), 2).toString());
            cbxcedulaCM.setSelectedItem(tblCM.getValueAt(tblCM.getSelectedRow(), 3).toString());
            txttemperaturaCM.setText(tblCM.getValueAt(tblCM.getSelectedRow(), 4).toString());
            txtpresionCM.setText(tblCM.getValueAt(tblCM.getSelectedRow(), 5).toString());
            txtsaturacionCM.setText(tblCM.getValueAt(tblCM.getSelectedRow(), 6).toString());
            txtmotivoCM.setText(tblCM.getValueAt(tblCM.getSelectedRow(), 7).toString());
            btnregistrarCM.setEnabled(false);
            btnmodificarCM.setEnabled(true);
            btnverexamenCM.setEnabled(true);
            btnverhistorialCM.setEnabled(true);
            cbxcedulaCM.setEnabled(false);
            cbxhistorialCM.setEnabled(false);
        } catch (Exception e) {
        }
    }

    /**
     * Metodo para deshabilitar los botones de modificacion de datos de una
     * consulta medica
     */
    private void deshabilitarModificacion() {
        tblCM.clearSelection();
        btnregistrarCM.setEnabled(true);
        btnmodificarCM.setEnabled(false);
        btnverexamenCM.setEnabled(false);
        btnverhistorialCM.setEnabled(false);
        cbxcedulaCM.setEnabled(true);
        cbxhistorialCM.setEnabled(true);
    }

    /**
     * Metodo para visualizar la lista de examenes que contiene una consulta
     * medica seleccionada
     */
    private void verExamen() {
        Frm_Examen verExamen = new Frm_Examen();
        verExamen.mostrarExamen((Integer) tblCM.getValueAt(tblCM.getSelectedRow(), 0));
        verExamen.setVisible(true);
        verExamen.setDefaultCloseOperation(HIDE_ON_CLOSE);
    }

    /**
     * Metodo para buscar un historial paciente que coincida con el numero de
     * historal indicado
     *
     * @return
     */
    private ListaSimple<HistorialPaciente> buscarHistorial() {
        return historialDao.buscar(tblCM.getValueAt(tblCM.getSelectedRow(), 2).toString(), HistorialPacienteDao.NROHISTORIACLINICA);
    }

    /**
     * Metodo para visualizar el historial paciente que contiene una consulta
     * medica seleccionada
     */
    private void verHistorial() {
        try {
            String id = buscarHistorial().extraer().getId().toString();
            String nroHistoriaClinica = buscarHistorial().extraer().getNroDeHistoriaClinica().toString();
            String nombre = buscarHistorial().extraer().getPaciente().getNombre();
            String apellido = buscarHistorial().extraer().getPaciente().getApellido();
            String grupoSanguineo = buscarHistorial().extraer().getGrupoSanguineo();
            String habito = buscarHistorial().extraer().getHabito();
            String alergias = buscarHistorial().extraer().getAlergias();
            String enfermedad = buscarHistorial().extraer().getEnfermedad();
            String enfermadadH = buscarHistorial().extraer().getEnfermedadHereditaria();
            int fila = historialDao.posicion(buscarHistorial().extraer());
            Frm_VerActualizarHistorialPaciente frmVHP = new Frm_VerActualizarHistorialPaciente(this, true, id, nroHistoriaClinica, nombre, apellido, grupoSanguineo, habito, alergias, enfermedad, enfermadadH, fila);
            frmVHP.setVisible(true);
        } catch (Exception e) {
        }
    }

    /**
     * Metodo para mostrar una lista de consultas medicas que coincidan con el
     * id ingresado
     *
     * @param id es el id a buscar en la lista
     */
    void mostrarConsulta(Integer id) {
        try {
            modelo.setLista(consultaDao.buscar(id));
            tblCM.setModel(modelo);
            tblCM.updateUI();
            habilitarModificacion();
            txtmotivoCM.setEditable(false);
            txtpresionCM.setEditable(false);
            txtsaturacionCM.setEditable(false);
            txttemperaturaCM.setEditable(false);
            btnregistrarCM.setVisible(false);
            btnmodificarCM.setVisible(false);
            btncancelarCM.setVisible(false);
            cbxcedulaCM.setEnabled(false);
            cbxhistorialCM.setEnabled(false);
            tblCM.setEnabled(false);
            btnactualizarCM1.setVisible(false);
            btnactualizarCM2.setVisible(true);
            aux = id;
        } catch (Exception ex) {
            Logger.getLogger(Frm_ConsultaMedica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtmotivoCM = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        txttemperaturaCM = new javax.swing.JTextField();
        txtpresionCM = new javax.swing.JTextField();
        txtsaturacionCM = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnregistrarCM = new javax.swing.JButton();
        btnmodificarCM = new javax.swing.JButton();
        btncancelarCM = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblCM = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cbxcedulaCM = new javax.swing.JComboBox<>();
        cbxhistorialCM = new javax.swing.JComboBox<>();
        btnverexamenCM = new javax.swing.JButton();
        btnverhistorialCM = new javax.swing.JButton();
        btnactualizarCM1 = new javax.swing.JButton();
        btnactualizarCM2 = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(130, 161, 177));
        setResizable(false);
        setSize(new java.awt.Dimension(647, 390));

        jPanel1.setBackground(new java.awt.Color(130, 161, 177));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CONSULTA MEDICA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        txtmotivoCM.setColumns(20);
        txtmotivoCM.setLineWrap(true);
        txtmotivoCM.setRows(5);
        txtmotivoCM.setWrapStyleWord(true);
        jScrollPane1.setViewportView(txtmotivoCM);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Motivo de consulta");

        txttemperaturaCM.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txttemperaturaCMKeyTyped(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Temperatura");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Presion arterial");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Saturación de oxigeno");

        btnregistrarCM.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnregistrarCM.setText("Registrar");
        btnregistrarCM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnregistrarCMActionPerformed(evt);
            }
        });

        btnmodificarCM.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnmodificarCM.setText("Modificar");
        btnmodificarCM.setEnabled(false);
        btnmodificarCM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarCMActionPerformed(evt);
            }
        });

        btncancelarCM.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btncancelarCM.setText("Cancelar");
        btncancelarCM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarCMActionPerformed(evt);
            }
        });

        tblCM.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblCM.getTableHeader().setReorderingAllowed(false);
        tblCM.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCMMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tblCM);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Cedula medico");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Nro Historia Clinica");

        btnverexamenCM.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnverexamenCM.setText("Ver examenes");
        btnverexamenCM.setEnabled(false);
        btnverexamenCM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnverexamenCMActionPerformed(evt);
            }
        });

        btnverhistorialCM.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnverhistorialCM.setText("Ver historial");
        btnverhistorialCM.setEnabled(false);
        btnverhistorialCM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnverhistorialCMActionPerformed(evt);
            }
        });

        btnactualizarCM1.setBackground(new java.awt.Color(0, 0, 102));
        btnactualizarCM1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnactualizarCM1.setToolTipText("Actualizar datos");
        btnactualizarCM1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnactualizarCM1ActionPerformed(evt);
            }
        });

        btnactualizarCM2.setBackground(new java.awt.Color(153, 0, 0));
        btnactualizarCM2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnactualizarCM2.setToolTipText("Actualizar datos");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(39, 39, 39)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtpresionCM, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                                    .addComponent(txtsaturacionCM)
                                    .addComponent(txttemperaturaCM, javax.swing.GroupLayout.Alignment.LEADING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 74, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(39, 39, 39)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cbxhistorialCM, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cbxcedulaCM, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jScrollPane3)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(btnregistrarCM, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(btnverhistorialCM, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnmodificarCM, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(btnverexamenCM, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(btncancelarCM, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnactualizarCM2, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnactualizarCM1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txttemperaturaCM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtpresionCM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(cbxcedulaCM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(cbxhistorialCM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtsaturacionCM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnregistrarCM)
                    .addComponent(btnmodificarCM)
                    .addComponent(btncancelarCM)
                    .addComponent(btnverexamenCM)
                    .addComponent(btnverhistorialCM))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnactualizarCM1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnactualizarCM2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnregistrarCMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnregistrarCMActionPerformed
        guardar();
        cargarTabla();
        deshabilitarModificacion();
    }//GEN-LAST:event_btnregistrarCMActionPerformed

    private void btncancelarCMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarCMActionPerformed
        limpiar();
        deshabilitarModificacion();
    }//GEN-LAST:event_btncancelarCMActionPerformed

    private void tblCMMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCMMouseClicked
        if (tblCM.isEnabled()) {
            habilitarModificacion();
        }
    }//GEN-LAST:event_tblCMMouseClicked

    private void btnmodificarCMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarCMActionPerformed
        modificar();
        cargarTabla();
    }//GEN-LAST:event_btnmodificarCMActionPerformed

    private void btnverexamenCMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnverexamenCMActionPerformed
        verExamen();
    }//GEN-LAST:event_btnverexamenCMActionPerformed

    private void btnverhistorialCMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnverhistorialCMActionPerformed
        verHistorial();
    }//GEN-LAST:event_btnverhistorialCMActionPerformed

    private void btnactualizarCM1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnactualizarCM1ActionPerformed
        cargarTabla();
    }//GEN-LAST:event_btnactualizarCM1ActionPerformed

    private void txttemperaturaCMKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttemperaturaCMKeyTyped
        validar.valDec(evt, txttemperaturaCM, 4);
    }//GEN-LAST:event_txttemperaturaCMKeyTyped

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_ConsultaMedica().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnactualizarCM1;
    private javax.swing.JButton btnactualizarCM2;
    private javax.swing.JButton btncancelarCM;
    private javax.swing.JButton btnmodificarCM;
    private javax.swing.JButton btnregistrarCM;
    private javax.swing.JButton btnverexamenCM;
    private javax.swing.JButton btnverhistorialCM;
    private javax.swing.JComboBox<String> cbxcedulaCM;
    private javax.swing.JComboBox<String> cbxhistorialCM;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable tblCM;
    private javax.swing.JTextArea txtmotivoCM;
    private javax.swing.JTextField txtpresionCM;
    private javax.swing.JTextField txtsaturacionCM;
    private javax.swing.JTextField txttemperaturaCM;
    // End of variables declaration//GEN-END:variables
}
