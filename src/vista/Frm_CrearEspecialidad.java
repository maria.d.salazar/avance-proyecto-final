/**
 * Anthony Luzuriaga
 */
package vista;

import controller.dao.EspecialidadDao1;
import controller.lista.ListaSimple;
import javax.swing.JOptionPane;
import vistas.modelo.ModeloTablaEspecialidad;

/**
 * @author Anthony Luzuriaga
 */
public class Frm_CrearEspecialidad extends javax.swing.JFrame {

    private EspecialidadDao1 especialidadDao = new EspecialidadDao1();
    private ModeloTablaEspecialidad modelo = new ModeloTablaEspecialidad();
    private String nombre;
    private Integer idE;

    public Frm_CrearEspecialidad() {
        initComponents();
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        ordenar();
    }

    /**
     * permite cargar la tabla de la lista de especialidades.
     */
    private void cargarTabla() {
        try {
            modelo.setLista(especialidadDao.listar());
            tblEspecialidad.setModel(modelo);
            tblEspecialidad.updateUI();
        } catch (Exception e) {
        }
    }
    
    /**
     * Este metodo permite ordenar de acuerdo al nombre o tipo de especialidad de manera ascendente.
     */
    private void ordenar() {
        modelo.setLista(especialidadDao.ordenar("tipo", ListaSimple.ASCENDENTE));
        tblEspecialidad.setModel(modelo);
        tblEspecialidad.updateUI();
    }
    
    /**
     * Este metodo permite buscar de acuerdo al nombre de la especialidad.
     */
    private void buscar() {
        modelo.setLista(especialidadDao.buscar(jxjNombreEspecialidad.getText(), EspecialidadDao1.TIPO));
        tblEspecialidad.setModel(modelo);
        tblEspecialidad.updateUI();
    }
    
    /**
     * Este  metodo permite obtener la posicion de la fila que se seleccione de la tabla.
     * @return la posicion de la fila seleccionada de la tabla.
     */
    public int seleccionarFila() {
        int n = tblEspecialidad.getSelectedRow();
        return n;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jxjNombreEspecialidad = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        botonGuardar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEspecialidad = new javax.swing.JTable();
        botonModificar = new javax.swing.JButton();
        botonActualizar = new javax.swing.JButton();
        botonActualizaTABLA = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel3.setText("Nombre");

        jxjNombreEspecialidad.setToolTipText("Ingrese el nombre de la especialidad");

        botonGuardar.setBackground(new java.awt.Color(102, 255, 0));
        botonGuardar.setText("Guardar");
        botonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(jxjNombreEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addComponent(botonGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jSeparator1)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jxjNombreEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(botonGuardar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        tblEspecialidad.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1"
            }
        ));
        jScrollPane1.setViewportView(tblEspecialidad);

        botonModificar.setText("Modificar");
        botonModificar.setToolTipText("Seleccione una fila de la tabla, para proceder a modificar");
        botonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonModificarActionPerformed(evt);
            }
        });

        botonActualizar.setText("Actualizar");
        botonActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonActualizarActionPerformed(evt);
            }
        });

        botonActualizaTABLA.setBackground(new java.awt.Color(255, 255, 51));
        botonActualizaTABLA.setText("Actualizar Tabla");
        botonActualizaTABLA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonActualizaTABLAActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(botonModificar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(botonActualizar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(botonActualizaTABLA))
                .addGap(16, 16, 16))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(botonActualizaTABLA)
                        .addGap(35, 35, 35)
                        .addComponent(botonActualizar)
                        .addGap(18, 18, 18)
                        .addComponent(botonModificar))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(130, 161, 177));

        jLabel1.setFont(new java.awt.Font("Amiri", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("ESPECIALIDADES");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(89, 89, 89))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    /**
     * Este metodo permite guardar los datos registrados en las cajas txt, de acuerdo al id.
     */
    private void guardar() {
        especialidadDao.getEspecialidad().setTipo(jxjNombreEspecialidad.getText());
        if (especialidadDao.getEspecialidad().getId() == null) {
            //guardar            
            if (especialidadDao.guardar()) {
                JOptionPane.showMessageDialog(this, "Se ha registrado correctamente");
                especialidadDao.setEspecialidad(null);
//                limpiar();
                ordenar();
            } else {
                JOptionPane.showMessageDialog(this, "No se pudo registrar");
            }
        } else {
            //modificar
        }
    }

    /**
     * Me guarda los datos, y comprueba que la caja de texto no sea nula
     * @param evt 
     */
    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarActionPerformed
        if(jxjNombreEspecialidad.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "Llene todos los campos");
        }else{
            guardar();
        }
    }//GEN-LAST:event_botonGuardarActionPerformed
    
    /**
     * Me permite modificar los datos de acuerdo a la fila seleccionada
     * @param evt 
     */
    private void botonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonModificarActionPerformed
        if (seleccionarFila() > -1) {
//            String id = tblEspecialidad.getValueAt(seleccionarFila(), 0).toString();
            nombre = tblEspecialidad.getValueAt(seleccionarFila(), 0).toString();
//            idE = Integer.valueOf(id);
            jxjNombreEspecialidad.setText(nombre);
        } else {
            JOptionPane.showMessageDialog(null, "Fila no seleccionada");
        }
    }//GEN-LAST:event_botonModificarActionPerformed
    
    /**
     * Este metodo me permite limpiar o recetear las cajas de texto a su valor original.
     */
    public void limpiarTXT() {
        jxjNombreEspecialidad.setText("");
//        especialidadDao.setEspecialidad(null);
        idE = null;
        nombre = null;
    }

    private void botonActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonActualizarActionPerformed
//        especialidadDao.setEspecialidad(especialidadDao.buscariD(idE));
        especialidadDao.setEspecialidad(especialidadDao.buscarnroNombre(nombre));
        especialidadDao.getEspecialidad().setTipo(jxjNombreEspecialidad.getText());
        especialidadDao.setEspecialidadAux(especialidadDao.getEspecialidad());
        if (especialidadDao.getEspecialidad().getTipo() != null) {
            if (especialidadDao.modi(especialidadDao.getEspecialidadAux(), seleccionarFila())) {
                JOptionPane.showMessageDialog(this, "Los cambios han sido guardados");
                limpiarTXT();
                cargarTabla();
            } else {
                JOptionPane.showMessageDialog(this, "Los cambios no se han podido guardar");
            }
        } else {
            System.out.println("VALO NULO");
        }
    }//GEN-LAST:event_botonActualizarActionPerformed

    private void botonActualizaTABLAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonActualizaTABLAActionPerformed
        cargarTabla();
    }//GEN-LAST:event_botonActualizaTABLAActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_CrearEspecialidad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_CrearEspecialidad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_CrearEspecialidad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_CrearEspecialidad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_CrearEspecialidad().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonActualizaTABLA;
    private javax.swing.JButton botonActualizar;
    private javax.swing.JButton botonGuardar;
    private javax.swing.JButton botonModificar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jxjNombreEspecialidad;
    private javax.swing.JTable tblEspecialidad;
    // End of variables declaration//GEN-END:variables
}
