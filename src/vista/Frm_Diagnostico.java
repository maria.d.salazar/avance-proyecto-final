package vista;

import controller.dao.ConsultaMedicaDao;
import controller.dao.DiagnosticoDao;
import java.awt.HeadlessException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import vistas.modelo.ModeloTablaDiagnostico;

/**
 *
 * @author Angel Román
 */
public class Frm_Diagnostico extends javax.swing.JFrame {

    private DiagnosticoDao diagnosticoDao = new DiagnosticoDao();
    private ConsultaMedicaDao consultaDao = new ConsultaMedicaDao();
    private ModeloTablaDiagnostico modelo = new ModeloTablaDiagnostico();
    Integer aux;

    public Frm_Diagnostico() {
        initComponents();
        this.setLocationRelativeTo(null);
        actualizarCombo();
        limpiar();
        cargarTabla();
        btnactualizarD2.setVisible(false);
    }

    /**
     * Metodo para cargar la tabla diagnostico
     */
    private void cargarTabla() {
        try {
            modelo.setLista(diagnosticoDao.listar());
            tbldiagnostico.setModel(modelo);
            tbldiagnostico.updateUI();
        } catch (Exception ex) {
            Logger.getLogger(Frm_ConsultaMedica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para actualizar combobox con la informacion de consulta medica
     */
    public void actualizarCombo() {
        try {
            DefaultComboBoxModel combo = new DefaultComboBoxModel();
            String[] nroHistoria = new String[consultaDao.listar().tamanio()];
            for (int i = 0; i < consultaDao.listar().tamanio(); i++) {
                nroHistoria[i] = consultaDao.listar().consultarDatoPos(i).getId().toString();
                combo.addElement(nroHistoria[i]);
                for (int j = 0; j < diagnosticoDao.listar().tamanio(); j++) {
                    if (nroHistoria[i].equals(diagnosticoDao.listar().consultarDatoPos(j).getNroConsulta().toString())) {
                        combo.removeElement(nroHistoria[i]);
                    }
                }
            }
            cbxconsultaD.setModel(combo);
        } catch (Exception e) {
        }
    }

    /**
     * Metodo para limpiar cuadros de texto
     */
    private void limpiar() {
        txtdescripcionD.setText("");
    }

    /**
     * Metodo para validar cuadros de texto
     *
     * @return true si los cuadros de texto no estan vacios, false si estan
     * vacios
     */
    private boolean validarDatos() {
        return !"".equals(txtdescripcionD.getText());
    }

    /**
     * Metodo para guardar el diagnostico con los datos ingresados
     */
    private void guardar() {
        try {
            diagnosticoDao.getDiagnostico().setDescripcion(txtdescripcionD.getText());
            diagnosticoDao.getDiagnostico().setNroConsulta(Integer.parseInt(cbxconsultaD.getSelectedItem().toString()));
            if (validarDatos() && diagnosticoDao.guardar()) {
                JOptionPane.showMessageDialog(this, "Se ha registrado correctamente");
                actualizarCombo();
            } else {
                JOptionPane.showMessageDialog(this, "Llene correctamente la informacion de todos los campos");
            }
        } catch (HeadlessException | NumberFormatException | NullPointerException e) {
            if (cbxconsultaD.getItemCount() == 0) {
                JOptionPane.showMessageDialog(null, "Todos los diagnosticos han sido realizados");
            } else {
                JOptionPane.showMessageDialog(null, "Llene correctamente la informacion de todos los campos");
            }
        }
    }

    /**
     * Metodo para modificar un diagnostico seleccionado en la tabla
     */
    private void modificar() {
        try {
            if (validarDatos()) {
                int id = (Integer) tbldiagnostico.getValueAt(tbldiagnostico.getSelectedRow(), 0);
                diagnosticoDao.setDiagnostico(diagnosticoDao.buscarDiagnostico(id));
                diagnosticoDao.getDiagnostico().setDescripcion(txtdescripcionD.getText());
                try {
                    diagnosticoDao.modificar(diagnosticoDao.getDiagnostico(), id - 1);
                    JOptionPane.showMessageDialog(this, "Se ha modificado correctamente");
                    deshabilitarModificacion();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, "No se pudo modificar");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Llene correctamente la informacion de todos los campos");
            }
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "Llene correctamente la informacion de todos los campos");
        }

    }

    /**
     * Metodo para llenar los cuadros de texto, habilitar los botones de vista y
     * modificacion de datos de un diagnostico seleccionado en la lista
     */
    private void habilitarModificacion() {
        try {
            txtdescripcionD.setText(tbldiagnostico.getValueAt(tbldiagnostico.getSelectedRow(), 2).toString());
            cbxconsultaD.setSelectedIndex(-1);
            cbxconsultaD.setEnabled(false);
            btnregistrarD.setEnabled(false);
            btnmodificarD.setEnabled(true);
            btnverconsultaD.setEnabled(true);
        } catch (Exception e) {
        }
    }

    /**
     * Metodo para deshabilitar los botones de modificacion de datos de un
     * diagnostico
     */
    private void deshabilitarModificacion() {
        tbldiagnostico.clearSelection();
        cbxconsultaD.setEnabled(true);
        btnregistrarD.setEnabled(true);
        btnmodificarD.setEnabled(false);
        btnverconsultaD.setEnabled(false);
    }

    /**
     * Metodo para visualizar la consulta medica que contiene un diagnostico
     * seleccionado
     */
    public void verConsulta() {
        Frm_ConsultaMedica verConsulta = new Frm_ConsultaMedica();
        verConsulta.mostrarConsulta((Integer) tbldiagnostico.getValueAt(tbldiagnostico.getSelectedRow(), 1));
        verConsulta.setVisible(true);
        verConsulta.setDefaultCloseOperation(HIDE_ON_CLOSE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtdescripcionD = new javax.swing.JTextArea();
        btnregistrarD = new javax.swing.JButton();
        btnmodificarD = new javax.swing.JButton();
        btncancelarD = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbldiagnostico = new javax.swing.JTable();
        cbxconsultaD = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        btnverconsultaD = new javax.swing.JButton();
        btnactualizarD1 = new javax.swing.JButton();
        btnactualizarD2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(130, 161, 177));
        setResizable(false);
        setSize(new java.awt.Dimension(551, 331));

        jPanel1.setBackground(new java.awt.Color(130, 161, 177));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DIAGNOSTICO DEL PACIENTE", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Descripcion");

        txtdescripcionD.setColumns(20);
        txtdescripcionD.setRows(5);
        jScrollPane1.setViewportView(txtdescripcionD);

        btnregistrarD.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnregistrarD.setText("Registrar");
        btnregistrarD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnregistrarDActionPerformed(evt);
            }
        });

        btnmodificarD.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnmodificarD.setText("Modificar");
        btnmodificarD.setEnabled(false);
        btnmodificarD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarDActionPerformed(evt);
            }
        });

        btncancelarD.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btncancelarD.setText("Cancelar");
        btncancelarD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarDActionPerformed(evt);
            }
        });

        tbldiagnostico.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbldiagnostico.getTableHeader().setReorderingAllowed(false);
        tbldiagnostico.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbldiagnosticoMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbldiagnostico);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Consulta Medica");

        btnverconsultaD.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnverconsultaD.setText("Ver consulta");
        btnverconsultaD.setEnabled(false);
        btnverconsultaD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnverconsultaDActionPerformed(evt);
            }
        });

        btnactualizarD1.setBackground(new java.awt.Color(0, 0, 102));
        btnactualizarD1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnactualizarD1.setToolTipText("Actualizar datos");
        btnactualizarD1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnactualizarD1ActionPerformed(evt);
            }
        });

        btnactualizarD2.setBackground(new java.awt.Color(153, 0, 0));
        btnactualizarD2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnactualizarD2.setToolTipText("Actualizar datos");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(cbxconsultaD, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnregistrarD, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(btnmodificarD, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnverconsultaD, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(btncancelarD, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 718, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnactualizarD2, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnactualizarD1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cbxconsultaD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnregistrarD)
                    .addComponent(btnmodificarD)
                    .addComponent(btncancelarD)
                    .addComponent(btnverconsultaD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnactualizarD1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnactualizarD2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 750, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 469, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnverconsultaDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnverconsultaDActionPerformed
        verConsulta();
    }//GEN-LAST:event_btnverconsultaDActionPerformed

    private void tbldiagnosticoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbldiagnosticoMouseClicked
        if (tbldiagnostico.isEnabled()) {
            habilitarModificacion();
        }
    }//GEN-LAST:event_tbldiagnosticoMouseClicked

    private void btncancelarDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarDActionPerformed
        limpiar();
        deshabilitarModificacion();
    }//GEN-LAST:event_btncancelarDActionPerformed

    private void btnmodificarDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarDActionPerformed
        modificar();
        cargarTabla();
    }//GEN-LAST:event_btnmodificarDActionPerformed

    private void btnregistrarDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnregistrarDActionPerformed
        guardar();
        cargarTabla();
        deshabilitarModificacion();
    }//GEN-LAST:event_btnregistrarDActionPerformed

    private void btnactualizarD1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnactualizarD1ActionPerformed
        cargarTabla();
    }//GEN-LAST:event_btnactualizarD1ActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_Diagnostico().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnactualizarD1;
    private javax.swing.JButton btnactualizarD2;
    private javax.swing.JButton btncancelarD;
    private javax.swing.JButton btnmodificarD;
    private javax.swing.JButton btnregistrarD;
    private javax.swing.JButton btnverconsultaD;
    private javax.swing.JComboBox<String> cbxconsultaD;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tbldiagnostico;
    private javax.swing.JTextArea txtdescripcionD;
    // End of variables declaration//GEN-END:variables
}
