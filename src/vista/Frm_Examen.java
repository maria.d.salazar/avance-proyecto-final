package vista;

import controller.dao.ConsultaMedicaDao;
import controller.dao.ExamenDao;
import java.awt.HeadlessException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import vistas.modelo.ModeloTablaExamen;

/**
 *
 * @author Angel Román
 */
public class Frm_Examen extends javax.swing.JFrame {

    private ExamenDao examenDao = new ExamenDao();
    private ConsultaMedicaDao consultaDao = new ConsultaMedicaDao();
    private ModeloTablaExamen modelo = new ModeloTablaExamen();
    Integer aux;

    public Frm_Examen() {
        initComponents();
        this.setLocationRelativeTo(null);
        actualizarCombo();
        limpiar();
        cargarTabla();
        btnactualizarE2.setVisible(false);
    }

    /**
     * Metodo para cargar la tabla examen
     */
    private void cargarTabla() {
        try {
            modelo.setLista(examenDao.listar());
            tblexamen.setModel(modelo);
            tblexamen.updateUI();
        } catch (Exception e) {
        }
    }

    /**
     * Metodo para actualizar combobox con la informacion de consulta medica
     */
    public void actualizarCombo() {
        try {
            DefaultComboBoxModel combo = new DefaultComboBoxModel();
            String[] nroHistoria = new String[consultaDao.listar().tamanio()];
            for (int i = 0; i < consultaDao.listar().tamanio(); i++) {
                nroHistoria[i] = consultaDao.listar().consultarDatoPos(i).getId().toString();
                combo.addElement(nroHistoria[i]);
            }
            cbxconsultaE.setModel(combo);
        } catch (Exception ex) {
            Logger.getLogger(Frm_ConsultaMedica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para limpiar cuadros de texto y reestablecer combobox
     */
    private void limpiar() {
        txttipoE.setText("");
        txtnombreE.setText("");
        txtdescripcionE.setText("");
        cbxconsultaE.setSelectedIndex(-1);
    }

    /**
     * Metodo para validar cuadros de texto
     *
     * @return true si los cuadros de texto no estan vacios, false si estan
     * vacios
     */
    private boolean validarDatos() {
        return !("".equals(txtdescripcionE.getText()) || "".equals(txtnombreE.getText()) || "".equals(txttipoE.getText()));
    }

    /**
     * metodo para guardat el examen con los datos ingresados
     */
    private void guardar() {
        try {
            examenDao.getExamen().setNroConsulta(Integer.parseInt(cbxconsultaE.getSelectedItem().toString()));
            examenDao.getExamen().setFecha(fechaActual());
            examenDao.getExamen().setNombre(txtnombreE.getText());
            examenDao.getExamen().setTipo(txttipoE.getText());
            examenDao.getExamen().setDescripcion(txtdescripcionE.getText());
            if (validarDatos() && examenDao.guardar()) {
                JOptionPane.showMessageDialog(this, "Se ha registrado correctamente");
            } else {
                JOptionPane.showMessageDialog(this, "Llene correctamente la informacion de todos los campos");
            }
        } catch (HeadlessException | NumberFormatException | NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Llene correctamente la informacion de todos los campos");
        }
    }

    /**
     * Metodo para modificar un examen seleccionado en la tabla
     */
    private void modificar() {
        try {
            if (validarDatos()) {
                int id = (Integer) tblexamen.getValueAt(tblexamen.getSelectedRow(), 0);
                examenDao.getExamen().setNroConsulta(Integer.parseInt(cbxconsultaE.getSelectedItem().toString()));
                examenDao.setExamen(examenDao.buscarExamen(id));
                examenDao.getExamen().setNombre(txtnombreE.getText());
                examenDao.getExamen().setTipo(txttipoE.getText());
                examenDao.getExamen().setDescripcion(txtdescripcionE.getText());
                try {
                    examenDao.modificar(examenDao.getExamen(), id - 1);
                    JOptionPane.showMessageDialog(this, "Se ha modificado correctamente");
                    deshabilitarModificacion();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, "No se pudo modificar");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Llene correctamente la informacion de todos los campos");
            }
        } catch (HeadlessException | NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Llene correctamente la informacion de todos los campos");
        }
    }

    /**
     * Metodo para obtener la fecha actual del sistema
     *
     * @return la fecha obtenida
     */
    public String fechaActual() {
        LocalDateTime fechaLocal = LocalDateTime.now();
        int dia = fechaLocal.getDayOfMonth();
        int mes = fechaLocal.getMonthValue();
        int anio = fechaLocal.getYear();
        String fecha = dia + "/" + mes + "/" + anio;
        return fecha;
    }

    /**
     * Metodo para llenar los cuadros de texto, combobox, habilitar los botones
     * de modificacion de datos de un examen seleccionada en la lista
     */
    private void habilitarModificacion() {
        try {
            cbxconsultaE.setSelectedItem(tblexamen.getValueAt(tblexamen.getSelectedRow(), 1).toString());
            txttipoE.setText(tblexamen.getValueAt(tblexamen.getSelectedRow(), 3).toString());
            txtnombreE.setText(tblexamen.getValueAt(tblexamen.getSelectedRow(), 4).toString());
            txtdescripcionE.setText(tblexamen.getValueAt(tblexamen.getSelectedRow(), 5).toString());
            btnmodificarE.setEnabled(true);
            btnregistrarE.setEnabled(false);
        } catch (Exception e) {
        }
    }

    /**
     * Metodo para deshabilitar los botones de modificacion de datos de un
     * examen
     */
    private void deshabilitarModificacion() {
        tblexamen.clearSelection();
        btnmodificarE.setEnabled(false);
        btnregistrarE.setEnabled(true);
    }

    /**
     * Metodo para mostrar una lista de examenes que coincidan con el id
     * ingresado
     *
     * @param id es el id a buscar en la lista
     */
    void mostrarExamen(Integer id) {
        try {
            modelo.setLista(examenDao.buscar(id));
            tblexamen.setModel(modelo);
            tblexamen.updateUI();
            txttipoE.setEditable(false);
            txtnombreE.setEditable(false);
            txtdescripcionE.setEditable(false);
            btnregistrarE.setVisible(false);
            btnmodificarE.setVisible(false);
            btncancelarE.setVisible(false);
            cbxconsultaE.setEnabled(false);
            btnactualizarE1.setVisible(false);
            btnactualizarE2.setVisible(true);
            aux = id;
        } catch (Exception ex) {
            Logger.getLogger(Frm_ConsultaMedica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtnombreE = new javax.swing.JTextField();
        txttipoE = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtdescripcionE = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblexamen = new javax.swing.JTable();
        btnregistrarE = new javax.swing.JButton();
        btnmodificarE = new javax.swing.JButton();
        btncancelarE = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        cbxconsultaE = new javax.swing.JComboBox<>();
        btnactualizarE1 = new javax.swing.JButton();
        btnactualizarE2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(130, 161, 177));
        setResizable(false);
        setSize(new java.awt.Dimension(508, 360));

        jPanel1.setBackground(new java.awt.Color(130, 161, 177));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "EXAMEN", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Tipo");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Nombre");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Descripcion");

        txtdescripcionE.setColumns(20);
        txtdescripcionE.setLineWrap(true);
        txtdescripcionE.setRows(5);
        txtdescripcionE.setWrapStyleWord(true);
        jScrollPane1.setViewportView(txtdescripcionE);

        tblexamen.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblexamen.getTableHeader().setReorderingAllowed(false);
        tblexamen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblexamenMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tblexamen);

        btnregistrarE.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnregistrarE.setText("Registrar");
        btnregistrarE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnregistrarEActionPerformed(evt);
            }
        });

        btnmodificarE.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnmodificarE.setText("Modificar");
        btnmodificarE.setEnabled(false);
        btnmodificarE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarEActionPerformed(evt);
            }
        });

        btncancelarE.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btncancelarE.setText("Cancelar");
        btncancelarE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarEActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Consulta Medica");

        btnactualizarE1.setBackground(new java.awt.Color(0, 0, 102));
        btnactualizarE1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnactualizarE1.setToolTipText("Actualizar datos");
        btnactualizarE1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnactualizarE1ActionPerformed(evt);
            }
        });

        btnactualizarE2.setBackground(new java.awt.Color(153, 0, 0));
        btnactualizarE2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnactualizarE2.setToolTipText("Actualizar datos");
        btnactualizarE2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnactualizarE2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnregistrarE, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(181, 181, 181)
                        .addComponent(btnmodificarE, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(180, 180, 180)
                        .addComponent(btncancelarE, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(39, 39, 39)
                        .addComponent(txttipoE, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(39, 39, 39)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbxconsultaE, 0, 150, Short.MAX_VALUE)
                            .addComponent(txtnombreE))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnactualizarE2, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnactualizarE1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txttipoE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtnombreE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(cbxconsultaE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnregistrarE)
                    .addComponent(btnmodificarE)
                    .addComponent(btncancelarE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnactualizarE1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnactualizarE2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnregistrarEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnregistrarEActionPerformed
        guardar();
        cargarTabla();
        deshabilitarModificacion();
    }//GEN-LAST:event_btnregistrarEActionPerformed

    private void btncancelarEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarEActionPerformed
        limpiar();
        deshabilitarModificacion();
    }//GEN-LAST:event_btncancelarEActionPerformed

    private void btnmodificarEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarEActionPerformed
        modificar();
        cargarTabla();
    }//GEN-LAST:event_btnmodificarEActionPerformed

    private void tblexamenMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblexamenMouseClicked
        if (tblexamen.isEnabled()) {
            habilitarModificacion();
        }
    }//GEN-LAST:event_tblexamenMouseClicked

    private void btnactualizarE1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnactualizarE1ActionPerformed
        cargarTabla();
    }//GEN-LAST:event_btnactualizarE1ActionPerformed

    private void btnactualizarE2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnactualizarE2ActionPerformed
        mostrarExamen(aux);
    }//GEN-LAST:event_btnactualizarE2ActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_Examen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnactualizarE1;
    private javax.swing.JButton btnactualizarE2;
    private javax.swing.JButton btncancelarE;
    private javax.swing.JButton btnmodificarE;
    private javax.swing.JButton btnregistrarE;
    private javax.swing.JComboBox<String> cbxconsultaE;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tblexamen;
    private javax.swing.JTextArea txtdescripcionE;
    private javax.swing.JTextField txtnombreE;
    private javax.swing.JTextField txttipoE;
    // End of variables declaration//GEN-END:variables
}
