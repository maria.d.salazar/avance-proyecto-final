/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controller.Validaciones;
import controller.dao.EspecialidadDao1;
import controller.dao.PersonaDao;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import vistas.modelo.ModeloTablaMedicos;

/**
 *
 * @author Mary Salazar
 */
public class Frm_Medico extends javax.swing.JFrame {

    PersonaDao cuentaDao = new PersonaDao();
    EspecialidadDao1 especialidad = new EspecialidadDao1();
    private ModeloTablaMedicos modelo = new ModeloTablaMedicos();
    Validaciones validar = new Validaciones();

    /**
     * Creates new form Frm_Persona
     */
    public Frm_Medico() {
        initComponents();
        txtMedicoQuemado.setEnabled(false);
        cargarTabla();
        actualizarCombo();
    }

    private void cargarTabla() {
        try {
            modelo.setLista(cuentaDao.getListado2());
            tablaMedicos.setModel(modelo);
            tablaMedicos.updateUI();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al Cargar la Tabla");
        }
    }

    public void Limpiar() {
        txtnombre.setText(null);
        txtcedula.setText(null);
        txtapellido.setText(null);
        txtnacionalidad.setText(null);
        txtdireccion.setText(null);
        txtcelular.setText(null);
        txtciudad.setText(null);
        txtFechaNacimiento.setText(null);
        txtedad.setText(null);
        cboGenero.setSelectedIndex(0);
        cboProvincia.setSelectedIndex(0);
    }

    public void actualizarCombo() {
        try {
            DefaultComboBoxModel com1 = new DefaultComboBoxModel();
            String[] nombreEspecialidades = new String[especialidad.listar().tamanio()];
            for (int i = 0; i < especialidad.listar().tamanio(); i++) {
                nombreEspecialidades[i] = especialidad.listar().consultarDatoPos(i).getTipo();
                com1.addElement(nombreEspecialidades[i]);
            }
            cbxEspecialidad.setModel(com1);
        } catch (Exception ex) {
            Logger.getLogger(Frm_Medico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void guardarMedico() {
        if (txtcedula.getText().equals("") || txtapellido.getText().equals("") || txtnombre.getText().equals("")
                || txtcelular.getText().equals("") || txtnacionalidad.getText().equals("") || txtciudad.getText().equals("")
                || txtdireccion.getText().equals("") || cboProvincia.getSelectedItem().equals("Seleccione")
                || cboGenero.getSelectedItem().equals("Seleccione") || txtedad.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "LLene todos los campos");

        } else {
            cuentaDao.setPersona(null);
            cuentaDao.getPersona().setEspecialidad(especialidad.getEspecialidad());
            cuentaDao.getPersona().setCedula(txtcedula.getText());
            cuentaDao.getPersona().setNombre(txtnombre.getText());
            cuentaDao.getPersona().setApellido(txtapellido.getText());
            cuentaDao.getPersona().setCelular(txtcelular.getText());
            cuentaDao.getPersona().setCorreo(txtcorreo.getText());
            cuentaDao.getPersona().setNacionalidad(txtnacionalidad.getText());
            cuentaDao.getPersona().setCiudad(txtciudad.getText());
            cuentaDao.getPersona().setDireccion(txtdireccion.getText());
            cuentaDao.getPersona().setProvincia(cboProvincia.getSelectedItem().toString());
            cuentaDao.getPersona().setFechaNacimiento(txtFechaNacimiento.getText());
            cuentaDao.getPersona().setEdad(Integer.parseInt(txtedad.getText()));
            cuentaDao.getPersona().setRol(txtMedicoQuemado.getText());
            cuentaDao.getPersona().setSexo(cboGenero.getSelectedItem().toString());
            cuentaDao.getPersona().getEspecialidad().setTipo(cbxEspecialidad.getSelectedItem().toString());
            cuentaDao.getPersona().getCuenta().setUsuario(txtusuario1.getText());
            cuentaDao.getPersona().getCuenta().setClave(txtcontra.getText());
            if (cuentaDao.getPersona().getId() == null) {
                if (cuentaDao.guardar()) {
                    JOptionPane.showMessageDialog(this, "Se ha registrado correctamente");
                    Limpiar();
                    cargarTabla();
                } else {
                    JOptionPane.showMessageDialog(this, "No registrado");
                }
            }
        }
    }

    public boolean comprobarCedula() {
        boolean f = true;
        cuentaDao.getListado2();
        String valor = txtcedula.getText();
        try {
            for (int i = 0; i < cuentaDao.getListado2().tamanio(); i++) {
                if (cuentaDao.getListado2().consultarDatoPos(i).getCedula().equals(valor)) {
                    f = false;
                }
            }
        } catch (Exception e) {

        }
        return f;
    }

    public boolean comprobarUsuarioMed() {
        boolean f = true;
        cuentaDao.getListado2();
        String usuario = txtusuario1.getText();
        try {
            for (int i = 0; i < cuentaDao.getListado2().tamanio(); i++) {
                if (cuentaDao.getListado2().consultarDatoPos(i).getCuenta().getUsuario().equals(usuario)) {
                    f = false;
                }
            }
        } catch (Exception e) {

        }
        return f;
    }

    public void verficarGuardar() {
        if (comprobarCedula()) {
            if (comprobarUsuarioMed()) {
                guardarMedico();
            } else {
                JOptionPane.showMessageDialog(null, "El USUARIO NO SE ENCUENTRA DISPONIBLE");
            }
        } else {
            JOptionPane.showMessageDialog(null, "LA CEDULA INGRESADA, YA EXISTE EN EL SISTEMA");
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtapellido = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        txtcedula = new javax.swing.JTextField();
        txtciudad = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cboGenero = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtcelular = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        cboProvincia = new javax.swing.JComboBox<>();
        txtnacionalidad = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtcorreo = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaMedicos = new javax.swing.JTable();
        jLabel15 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        txtcontra = new javax.swing.JPasswordField();
        jLabel18 = new javax.swing.JLabel();
        txtusuario1 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        txtMedicoQuemado = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        cbxEspecialidad = new javax.swing.JComboBox<>();
        txtFechaNacimiento = new javax.swing.JTextField();
        txtedad = new javax.swing.JTextField();

        jTable1.setBorder(javax.swing.BorderFactory.createTitledBorder("LIstado de Pacientes Registrados"));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(130, 161, 177));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Registro del Personal");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 0, 390, 60));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Apellido");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 270, -1, 30));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Nombre");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 310, -1, 30));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Cédula");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 230, -1, 30));

        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtapellidoKeyTyped(evt);
            }
        });
        jPanel1.add(txtapellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 270, 200, 30));

        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnombreKeyTyped(evt);
            }
        });
        jPanel1.add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 310, 200, 30));

        txtcedula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcedulaActionPerformed(evt);
            }
        });
        txtcedula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcedulaKeyTyped(evt);
            }
        });
        jPanel1.add(txtcedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 230, 200, 30));

        txtciudad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtciudadKeyTyped(evt);
            }
        });
        jPanel1.add(txtciudad, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 390, 200, 30));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Edad");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 390, -1, 30));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Fecha de Nacimiento");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 350, -1, 30));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Dirección");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 350, 70, 30));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Género");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 230, -1, -1));

        cboGenero.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboGenero.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "Masculino", "Femenino" }));
        jPanel1.add(cboGenero, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 220, 180, 30));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Celular");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 440, -1, -1));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("Correo Electronico");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 240, -1, 20));

        txtcelular.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcelularKeyTyped(evt);
            }
        });
        jPanel1.add(txtcelular, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 430, 200, 30));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setText("Ciudad");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 400, -1, -1));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setText("Provincia ");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 310, -1, 30));

        cboProvincia.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboProvincia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "Azuay", "Guayas", "Pichincha", "Zamora Chinchipe", "Loja", "El Oro", "Esmeraldas", "Manabí", "Morona Santiago", "Sucumbios", "Riobamba" }));
        jPanel1.add(cboProvincia, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 310, 160, 30));

        txtnacionalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnacionalidadKeyTyped(evt);
            }
        });
        jPanel1.add(txtnacionalidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 270, 160, 30));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setText("Nacionalidad ");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 270, -1, 30));

        txtcorreo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcorreoKeyTyped(evt);
            }
        });
        jPanel1.add(txtcorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 230, 160, 30));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setText("Cedula");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 160, -1, 30));
        jPanel1.add(jTextField9, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 160, 200, 30));

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setText("Buscar");
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 160, 90, 30));

        btnGuardar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnGuardar.setText("Registrar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jPanel1.add(btnGuardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 490, 100, 30));

        btnCancelar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCancelar.setText("Cancelar");
        jPanel1.add(btnCancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 490, 100, 30));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "LISTADO DEL PERSONAL", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tablaMedicos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cedula", "Apellido", "Nombre", "Genero", "Edad", "Correo Electronico"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaMedicos.setGridColor(new java.awt.Color(255, 255, 255));
        jScrollPane3.setViewportView(tablaMedicos);
        if (tablaMedicos.getColumnModel().getColumnCount() > 0) {
            tablaMedicos.getColumnModel().getColumn(0).setResizable(false);
            tablaMedicos.getColumnModel().getColumn(1).setResizable(false);
            tablaMedicos.getColumnModel().getColumn(2).setResizable(false);
            tablaMedicos.getColumnModel().getColumn(3).setResizable(false);
            tablaMedicos.getColumnModel().getColumn(4).setResizable(false);
            tablaMedicos.getColumnModel().getColumn(5).setResizable(false);
        }

        jPanel2.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 990, 100));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 550, 1020, 140));

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/google_forms_96px.png"))); // NOI18N
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 60, 90, 80));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Información de la Cuenta", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(131, 161, 171));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setText("Usuario");
        jPanel4.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 20, -1, 30));

        txtcontra.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jPanel4.add(txtcontra, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 250, 30));

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel18.setText("Contraseña");
        jPanel4.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 80, -1, 30));

        txtusuario1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtusuario1KeyTyped(evt);
            }
        });
        jPanel4.add(txtusuario1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 240, 30));

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 290, 170));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 320, 320, 210));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setText("Rol");
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 270, -1, 30));

        txtdireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtdireccionKeyTyped(evt);
            }
        });
        jPanel1.add(txtdireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 350, 200, 30));

        txtMedicoQuemado.setText("MEDICO");
        jPanel1.add(txtMedicoQuemado, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 270, 180, 30));

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setText("Especialidad");
        jPanel1.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 430, -1, 30));

        cbxEspecialidad.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cbxEspecialidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione" }));
        jPanel1.add(cbxEspecialidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 430, 160, 30));

        txtFechaNacimiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFechaNacimientoActionPerformed(evt);
            }
        });
        txtFechaNacimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtFechaNacimientoKeyTyped(evt);
            }
        });
        jPanel1.add(txtFechaNacimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 350, 160, 30));

        txtedad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtedadKeyTyped(evt);
            }
        });
        jPanel1.add(txtedad, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 390, 160, 30));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1070, 770));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        verficarGuardar();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void txtcedulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcedulaActionPerformed
        validar.validadorDeCedula(txtcedula.getText(), txtcedula);
    }//GEN-LAST:event_txtcedulaActionPerformed

    private void txtcedulaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcedulaKeyTyped
        // TODO add your handling code here:
        validar.valNum(evt, txtcedula, 10);
        validar.EnterAJtexFiel(txtapellido, evt);
    }//GEN-LAST:event_txtcedulaKeyTyped

    private void txtapellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtapellidoKeyTyped
        validar.Mayusculas(txtapellido.getText(), evt);
        validar.valLetr(evt, txtapellido, 50);
        validar.EnterAJtexFiel(txtnombre, evt);
    }//GEN-LAST:event_txtapellidoKeyTyped

    private void txtnombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyTyped
        validar.Mayusculas(txtnombre.getText(), evt);
        validar.valLetr(evt, txtnombre, 30);
        validar.EnterAJtexFiel(txtdireccion, evt);
    }//GEN-LAST:event_txtnombreKeyTyped

    private void txtdireccionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionKeyTyped
        validar.Mayusculas(txtdireccion.getText(), evt);
        validar.valLetr(evt, txtdireccion, 60);
        validar.EnterAJtexFiel(txtciudad, evt);
    }//GEN-LAST:event_txtdireccionKeyTyped

    private void txtciudadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtciudadKeyTyped
        validar.Mayusculas(txtciudad.getText(), evt);
        validar.valLetr(evt, txtciudad, 40);
        validar.EnterAJtexFiel(txtcelular, evt);
    }//GEN-LAST:event_txtciudadKeyTyped

    private void txtcelularKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcelularKeyTyped
        validar.valNum(evt, txtcelular, 10);
        validar.EnterAJtexFiel(txtcorreo, evt);
    }//GEN-LAST:event_txtcelularKeyTyped

    private void txtcorreoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcorreoKeyTyped
        validar.EnterAJtexFiel(txtnacionalidad, evt);
    }//GEN-LAST:event_txtcorreoKeyTyped

    private void txtnacionalidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnacionalidadKeyTyped
        validar.Mayusculas(txtnacionalidad.getText(), evt);
        validar.valLetr(evt, txtnacionalidad, 60);
        validar.EnterAJtexFiel(txtFechaNacimiento, evt);
    }//GEN-LAST:event_txtnacionalidadKeyTyped

    private void txtusuario1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtusuario1KeyTyped
        validar.Mayusculas(txtusuario1.getText(), evt);
        validar.valLetr(evt, txtusuario1, 10);
        validar.EnterAJtexFiel(txtcontra, evt);
    }//GEN-LAST:event_txtusuario1KeyTyped

    private void txtFechaNacimientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFechaNacimientoActionPerformed

    }//GEN-LAST:event_txtFechaNacimientoActionPerformed

    private void txtFechaNacimientoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFechaNacimientoKeyTyped
        validar.EnterAJtexFiel(txtedad, evt);
    }//GEN-LAST:event_txtFechaNacimientoKeyTyped

    private void txtedadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtedadKeyTyped
        // TODO add your handling code here:
        validar.valNum(evt, txtedad, 2);
        validar.EnterAJtexFiel(txtedad, evt);
    }//GEN-LAST:event_txtedadKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_Medico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_Medico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_Medico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_Medico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_Medico().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JComboBox<String> cboGenero;
    private javax.swing.JComboBox<String> cboProvincia;
    private javax.swing.JComboBox<String> cbxEspecialidad;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField jTextField9;
    private javax.swing.JTable tablaMedicos;
    private javax.swing.JTextField txtFechaNacimiento;
    private javax.swing.JTextField txtMedicoQuemado;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JTextField txtcedula;
    private javax.swing.JTextField txtcelular;
    private javax.swing.JTextField txtciudad;
    private javax.swing.JPasswordField txtcontra;
    private javax.swing.JTextField txtcorreo;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtedad;
    private javax.swing.JTextField txtnacionalidad;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txtusuario1;
    // End of variables declaration//GEN-END:variables
}
