/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controller.Validaciones;
import controller.dao.PersonaDao;
import controller.lista.ListaSimple;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Persona;
import vistas.modelo.ModeloTablaPersonas;

/**
 *
 * @author Mary Salazar
 */
public class Frm_Persona extends javax.swing.JFrame {

    private PersonaDao pacienteDao = new PersonaDao();
    Validaciones validar = new Validaciones();
    private ModeloTablaPersonas modelotablaAdmin = new ModeloTablaPersonas();
    private String cedula;
    private Integer idE;
    private int edad;
    private String ape;
    private String nomb;
    private String dire;
    private String celu;
    private String corre;
    private String nacio;
    private String ciud;
    private String provi;
    private String sex;
    private String disca;
    private String fech;

    /**
     * Creates new form Frm_Persona
     */
    public Frm_Persona() {
        initComponents();
        this.setLocationRelativeTo(null);
        txtFechaPaciente.setText("dd/mm/aaaa");
        txtPacienteQuemado.setEnabled(false);
        cargarTabla();
        btnmodificar1.setVisible(false);
        btnactualizar.setVisible(false);
    }

    private void cargarTabla() {
        try {
            modelotablaAdmin.setLista(pacienteDao.getListado3());
            tablaPaciente.setModel(modelotablaAdmin);
            tablaPaciente.updateUI();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al Cargar la Tabla");
        }
    }

    public void Limpiar() {
        txtnombre.setText(null);
        txtcedula.setText(null);
        txtapellido.setText(null);
        txtnacionalidad.setText(null);
        txtdireccion.setText(null);
        txtcorreoP.setText(null);
        txtcelular.setText(null);
        txtciudad.setText(null);
        txtFechaPaciente.setText("dd/mm/aaaa");
        cbodiscapacidad.setSelectedIndex(0);
        cboGenero.setSelectedIndex(0);
        cboedad.setSelectedIndex(0);
        cboprovincia.setSelectedIndex(0);
        idE = null;
        cedula = null;
        nomb = null;
        ape = null;
        celu = null;
        dire = null;
        ciud = null;
        corre = null;
        nacio = null;
        provi = null;
        sex = null;
        disca = null;

    }

    public void guardar() {
        if (txtcedula.getText().equals("") || txtapellido.getText().equals("") || txtnombre.getText().equals("")
                || txtcelular.getText().equals("") || txtnacionalidad.getText().equals("") || txtciudad.getText().equals("")
                || txtdireccion.getText().equals("") || cboprovincia.getSelectedItem().equals("Seleccione") || cbodiscapacidad.getSelectedItem().equals("Seleccione")
                || cboGenero.getSelectedItem().equals("Seleccione") || cboedad.getSelectedItem().equals("0")||txtFechaPaciente.getText().equals("dd/mm/aaaa")) {
            JOptionPane.showMessageDialog(this, "LLene todos los campos");

        } else {
            pacienteDao.getPersona().setCedula(txtcedula.getText());
            pacienteDao.getPersona().setNombre(txtnombre.getText());
            pacienteDao.getPersona().setApellido(txtapellido.getText());
            pacienteDao.getPersona().setCelular(txtcelular.getText());
            pacienteDao.getPersona().setCorreo(txtcorreoP.getText());
            pacienteDao.getPersona().setNacionalidad(txtnacionalidad.getText());
            pacienteDao.getPersona().setCiudad(txtciudad.getText());
            pacienteDao.getPersona().setDireccion(txtdireccion.getText());
            pacienteDao.getPersona().setProvincia(cboprovincia.getSelectedItem().toString());
            pacienteDao.getPersona().setRol(txtPacienteQuemado.getText());
            pacienteDao.getPersona().setSexo(cboGenero.getSelectedItem().toString());
            pacienteDao.getPersona().setFechaNacimiento(txtFechaPaciente.getText());
            pacienteDao.getPersona().setDiscapcidad(cbodiscapacidad.getSelectedItem().toString());
            pacienteDao.getPersona().setEdad(Integer.parseInt(cboedad.getSelectedItem().toString()));
            if (pacienteDao.getPersona().getId() == null) {
                if (pacienteDao.guardar()) {
                    JOptionPane.showMessageDialog(this, "Se ha registrado correctamente");
                    pacienteDao.setPersona(null);
                    cargarTabla();
                    Limpiar();
                } else {
                    JOptionPane.showMessageDialog(this, "No registrado");
                }
            }
        }
    }

    private void Buscar() {
        modelotablaAdmin.setLista(new ListaSimple());
        if (pacienteDao.buscarCedula(txtBuscarCed.getText()) != null) {
            modelotablaAdmin.getLista().insertarDato(pacienteDao.buscarCedula(txtBuscarCed.getText()));
        }
        tablaPaciente.setModel(modelotablaAdmin);
        tablaPaciente.updateUI();
    }

    public int seleccionarFila() {
        int n = tablaPaciente.getSelectedRow();
        return n;
    }
    private Persona retornarPersona() {
        try {
//            hpdao.setHistorialPAUX(null);
//            return hpdao.buscarID(this.ids);
            return pacienteDao.buscarCedula(cedula);
        } catch (Exception ex) {
            Logger.getLogger(Frm_VerActualizarHistorialPaciente.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtapellido = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        txtcedula = new javax.swing.JTextField();
        txtciudad = new javax.swing.JTextField();
        txtdireccion = new javax.swing.JTextField();
        cboedad = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cboGenero = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtcelular = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        cboprovincia = new javax.swing.JComboBox<>();
        txtnacionalidad = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtPacienteQuemado = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtBuscarCed = new javax.swing.JTextField();
        cbxBuscarCed = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnactualizar = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaPaciente = new javax.swing.JTable();
        txtcorreoP = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        cbodiscapacidad = new javax.swing.JComboBox<>();
        jLabel17 = new javax.swing.JLabel();
        btnmodificar1 = new javax.swing.JButton();
        txtFechaPaciente = new javax.swing.JTextField();

        jTable1.setBorder(javax.swing.BorderFactory.createTitledBorder("LIstado de Pacientes Registrados"));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(130, 161, 177));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Registro de Paciente");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, 390, 60));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Apellido");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, -1, 30));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Nombre");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 250, -1, 30));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Cédula");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, -1, 30));

        txtapellido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtapellidoActionPerformed(evt);
            }
        });
        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtapellidoKeyTyped(evt);
            }
        });
        jPanel1.add(txtapellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 210, 200, 30));

        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnombreKeyTyped(evt);
            }
        });
        jPanel1.add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 250, 200, 30));

        txtcedula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcedulaActionPerformed(evt);
            }
        });
        txtcedula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcedulaKeyTyped(evt);
            }
        });
        jPanel1.add(txtcedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 170, 200, 30));

        txtciudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtciudadActionPerformed(evt);
            }
        });
        txtciudad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtciudadKeyTyped(evt);
            }
        });
        jPanel1.add(txtciudad, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 250, 180, 30));

        txtdireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtdireccionKeyTyped(evt);
            }
        });
        jPanel1.add(txtdireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 290, 200, 30));

        cboedad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "88", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", "111", "112", "113" }));
        jPanel1.add(cboedad, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 210, 160, 30));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Edad");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 210, -1, 30));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Fecha de Nacimiento");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 170, -1, 30));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Dirección");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, 70, 30));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Rol");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 290, 40, 20));

        cboGenero.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboGenero.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "Masculino", "Femenino" }));
        jPanel1.add(cboGenero, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 330, 180, 30));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Discapacidad");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 250, -1, -1));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("Correo Electronico");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 177, -1, 20));

        txtcelular.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcelularKeyTyped(evt);
            }
        });
        jPanel1.add(txtcelular, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 330, 200, 30));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setText("Ciudad");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 260, -1, -1));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setText("Provincia ");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 290, -1, 30));

        cboprovincia.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboprovincia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "Azuay", "Guayas", "Pichincha", "Zamora Chinchipe", "Loja", "El Oro", "Esmeraldas", "Manabí", "Morona Santiago", "Sucumbios", "Riobamba" }));
        jPanel1.add(cboprovincia, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 290, 180, 30));

        txtnacionalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnacionalidadKeyTyped(evt);
            }
        });
        jPanel1.add(txtnacionalidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 210, 180, 30));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setText("Nacionalidad ");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 210, -1, 30));

        txtPacienteQuemado.setText("PACIENTE");
        txtPacienteQuemado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPacienteQuemadoActionPerformed(evt);
            }
        });
        jPanel1.add(txtPacienteQuemado, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 290, 160, 30));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setText("Cedula");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 100, -1, 30));
        jPanel1.add(txtBuscarCed, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 100, 200, 30));

        cbxBuscarCed.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cbxBuscarCed.setText("Buscar");
        cbxBuscarCed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxBuscarCedActionPerformed(evt);
            }
        });
        jPanel1.add(cbxBuscarCed, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 100, 90, 30));

        btnGuardar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnGuardar.setText("Registrar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jPanel1.add(btnGuardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 400, 100, 30));

        btnactualizar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnactualizar.setText("Actualizar");
        btnactualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnactualizarActionPerformed(evt);
            }
        });
        jPanel1.add(btnactualizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 400, 110, 30));

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btncancelar.setText("Cancelar");
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });
        jPanel1.add(btncancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 400, 100, 30));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "LISTADO DE PACIENTES REGISTRADOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tablaPaciente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CEDULA", "APELLIDO", "NOMBRE", "DIRECCION", "CELULAR", "CORREO ELECTRONICO", "CIUDAD", "PROVINCIA", "GENERO", "FECHA DE NACIMIENTO", "EDAD", "DISCAPACIDAD"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaPaciente.setGridColor(new java.awt.Color(255, 255, 255));
        jScrollPane3.setViewportView(tablaPaciente);
        if (tablaPaciente.getColumnModel().getColumnCount() > 0) {
            tablaPaciente.getColumnModel().getColumn(0).setResizable(false);
            tablaPaciente.getColumnModel().getColumn(1).setResizable(false);
            tablaPaciente.getColumnModel().getColumn(2).setResizable(false);
            tablaPaciente.getColumnModel().getColumn(3).setResizable(false);
            tablaPaciente.getColumnModel().getColumn(4).setResizable(false);
            tablaPaciente.getColumnModel().getColumn(5).setResizable(false);
            tablaPaciente.getColumnModel().getColumn(6).setResizable(false);
            tablaPaciente.getColumnModel().getColumn(7).setResizable(false);
            tablaPaciente.getColumnModel().getColumn(8).setResizable(false);
            tablaPaciente.getColumnModel().getColumn(9).setResizable(false);
            tablaPaciente.getColumnModel().getColumn(10).setResizable(false);
            tablaPaciente.getColumnModel().getColumn(11).setResizable(false);
        }

        jPanel2.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 940, 160));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 460, 970, 200));

        txtcorreoP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcorreoPKeyTyped(evt);
            }
        });
        jPanel1.add(txtcorreoP, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 170, 180, 30));

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setText("Celular");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 340, -1, -1));

        cbodiscapacidad.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cbodiscapacidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "No", "Si" }));
        jPanel1.add(cbodiscapacidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 250, 160, 30));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setText("Género");
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 340, -1, -1));

        btnmodificar1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnmodificar1.setText("Modificar");
        btnmodificar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificar1ActionPerformed(evt);
            }
        });
        jPanel1.add(btnmodificar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 400, 100, 30));
        jPanel1.add(txtFechaPaciente, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 170, 160, 30));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1020, 680));

        setSize(new java.awt.Dimension(1038, 723));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        guardar();

    }//GEN-LAST:event_btnGuardarActionPerformed

    private void txtcedulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcedulaActionPerformed
        // TODO add your handling code here:
        validar.validadorDeCedula(txtcedula.getText(), txtcedula);
    }//GEN-LAST:event_txtcedulaActionPerformed

    private void txtcedulaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcedulaKeyTyped
        validar.valNum(evt, txtcedula, 10);
        validar.EnterAJtexFiel(txtapellido, evt);
    }//GEN-LAST:event_txtcedulaKeyTyped

    private void txtapellidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtapellidoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtapellidoActionPerformed

    private void txtapellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtapellidoKeyTyped
        validar.Mayusculas(txtapellido.getText(), evt);
        validar.valLetr(evt, txtapellido, 50);
        validar.EnterAJtexFiel(txtnombre, evt);
    }//GEN-LAST:event_txtapellidoKeyTyped

    private void txtnombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyTyped
        validar.Mayusculas(txtnombre.getText(), evt);
        validar.valLetr(evt, txtnombre, 30);
        validar.EnterAJtexFiel(txtdireccion, evt);
    }//GEN-LAST:event_txtnombreKeyTyped

    private void txtPacienteQuemadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPacienteQuemadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPacienteQuemadoActionPerformed

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void txtciudadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtciudadKeyTyped
        // TODO add your handling code here:
        validar.Mayusculas(txtciudad.getText(), evt);
        validar.valLetr(evt, txtciudad, 40);
        validar.EnterAJtexFiel(txtciudad, evt);
    }//GEN-LAST:event_txtciudadKeyTyped

    private void txtcelularKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcelularKeyTyped
        validar.valNum(evt, txtcelular, 10);
        validar.EnterAJtexFiel(txtcorreoP, evt);
    }//GEN-LAST:event_txtcelularKeyTyped

    private void txtdireccionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionKeyTyped
        validar.Mayusculas(txtdireccion.getText(), evt);
        validar.valLetr(evt, txtdireccion, 60);
        validar.EnterAJtexFiel(txtcelular, evt);
    }//GEN-LAST:event_txtdireccionKeyTyped

    private void txtnacionalidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnacionalidadKeyTyped
        validar.Mayusculas(txtnacionalidad.getText(), evt);
        validar.valLetr(evt, txtFechaPaciente, 60);
        validar.EnterAJtexFiel(txtciudad, evt);
    }//GEN-LAST:event_txtnacionalidadKeyTyped

    private void txtcorreoPKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcorreoPKeyTyped
        // TODO add your handling code here:
        validar.EnterAJtexFiel(txtnacionalidad, evt);
    }//GEN-LAST:event_txtcorreoPKeyTyped

    private void txtciudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtciudadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtciudadActionPerformed

    private void cbxBuscarCedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxBuscarCedActionPerformed
        Buscar();
    }//GEN-LAST:event_cbxBuscarCedActionPerformed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        btnactualizar.setVisible(false);
        Limpiar();
        btnGuardar.setEnabled(true);
        txtcedula.setEnabled(true);
        txtnombre.setEnabled(true);
        txtapellido.setEnabled(true);
        txtnacionalidad.setEnabled(true);
        cboGenero.setEnabled(true);

    }//GEN-LAST:event_btncancelarActionPerformed

    private void btnmodificar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificar1ActionPerformed
        // TODO add your handling code here:
        btnGuardar.setEnabled(false);
        txtcedula.setEnabled(false);
        txtnombre.setEnabled(false);
        txtapellido.setEnabled(false);
        txtnacionalidad.setEnabled(false);
        cboGenero.setEnabled(false);
        btnactualizar.setVisible(true);
        if (seleccionarFila() > -1) {
            cedula = tablaPaciente.getValueAt(seleccionarFila(), 0).toString();
            txtcedula.setText(cedula);
            //
            ape = tablaPaciente.getValueAt(seleccionarFila(), 1).toString();
            txtapellido.setText(ape);
            //
            nomb = tablaPaciente.getValueAt(seleccionarFila(), 2).toString();
            txtnombre.setText(nomb);
            //
            fech = tablaPaciente.getValueAt(seleccionarFila(),3).toString();
            txtFechaPaciente.setText(fech);
            
            cboedad.setSelectedItem(tablaPaciente.getValueAt(seleccionarFila(), 4).toString());
            cboedad.setSelectedItem(cboedad.getSelectedItem());
            
            dire = tablaPaciente.getValueAt(seleccionarFila(), 5).toString();
            txtdireccion.setText(dire);
            //
            celu = tablaPaciente.getValueAt(seleccionarFila(), 6).toString();
            txtcelular.setText(celu);
            //
            corre = tablaPaciente.getValueAt(seleccionarFila(), 7).toString();
            txtcorreoP.setText(corre);
            //
            nacio = tablaPaciente.getValueAt(seleccionarFila(), 8).toString();
            txtnacionalidad.setText(nacio);
            //
            ciud = tablaPaciente.getValueAt(seleccionarFila(), 9).toString();
            txtciudad.setText(ciud);
            //
            cboprovincia.setSelectedItem(tablaPaciente.getValueAt(seleccionarFila(), 10).toString());
            //
            cboGenero.setSelectedItem(tablaPaciente.getValueAt(seleccionarFila(), 11).toString());
            //
            cbodiscapacidad.setSelectedItem(tablaPaciente.getValueAt(seleccionarFila(), 12).toString());
            //

        } else {
            JOptionPane.showMessageDialog(null, "Fila no seleccionada");
        }
    }//GEN-LAST:event_btnmodificar1ActionPerformed

    private void btnactualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnactualizarActionPerformed
        // TODO add your handling code here:
        btnGuardar.setEnabled(true);
        txtcedula.setEnabled(true);
        txtnombre.setEnabled(true);
        txtapellido.setEnabled(true);
        txtnacionalidad.setEnabled(true);
        cboGenero.setEnabled(true);
        pacienteDao.setPersona(retornarPersona());
        pacienteDao.getPersona().setCedula(txtcedula.getText());
        pacienteDao.getPersona().setApellido(txtapellido.getText());
        pacienteDao.getPersona().setNombre(txtnombre.getText());
        pacienteDao.getPersona().setEdad(cboedad.getSelectedIndex());
        pacienteDao.getPersona().setDireccion(txtdireccion.getText());
        pacienteDao.getPersona().setCelular(txtcelular.getText());
        pacienteDao.getPersona().setCorreo(txtcorreoP.getText());
        pacienteDao.getPersona().setNacionalidad(txtnacionalidad.getText());
        pacienteDao.getPersona().setCiudad(txtciudad.getText());
        pacienteDao.getPersona().setProvincia(cboprovincia.getSelectedItem().toString());
        pacienteDao.getPersona().setSexo(cboGenero.getSelectedItem().toString());
        pacienteDao.getPersona().setDiscapcidad(cbodiscapacidad.getSelectedItem().toString());
        pacienteDao.setPersonaAux(pacienteDao.getPersona());
        //
        if (pacienteDao.getPersona().getCedula() != null) {
            if (pacienteDao.modi(pacienteDao.getPersonaAux(), seleccionarFila())) {
                JOptionPane.showMessageDialog(this, "Los cambios han sido guardados");
                Limpiar();
                cargarTabla();
            } else {
                JOptionPane.showMessageDialog(this, "Los cambios no se han podido guardar");
            }
        } else {
            System.out.println("VALOR NULO");
        }
        btnactualizar.setVisible(false);
    }//GEN-LAST:event_btnactualizarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_Persona().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnactualizar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JButton btnmodificar1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.JComboBox<String> cboGenero;
    private javax.swing.JComboBox<String> cbodiscapacidad;
    private javax.swing.JComboBox<String> cboedad;
    private javax.swing.JComboBox<String> cboprovincia;
    private javax.swing.JButton cbxBuscarCed;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable tablaPaciente;
    private javax.swing.JTextField txtBuscarCed;
    private javax.swing.JTextField txtFechaPaciente;
    private javax.swing.JTextField txtPacienteQuemado;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JTextField txtcedula;
    private javax.swing.JTextField txtcelular;
    private javax.swing.JTextField txtciudad;
    private javax.swing.JTextField txtcorreoP;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtnacionalidad;
    private javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
}
