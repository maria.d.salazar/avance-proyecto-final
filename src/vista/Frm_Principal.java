package vista;

import controller.dao.PersonaDao;

public class Frm_Principal extends javax.swing.JFrame {

    PersonaDao persona = new PersonaDao();
    int tipo = 0;

    public Frm_Principal(int tip) {
        tipo = tip;
        initComponents();
        setExtendedState(MAXIMIZED_BOTH);
        activacionMenu();
    }

    private void activacionMenu() {
        if (tipo == 1) {
            JAdministrarPersona.setEnabled(true);
            jRegistroPaciente.setEnabled(true);
            jRegistroAdim.setEnabled(true);
            jRegistroPersonal.setEnabled(true);
            jRegistroEspecialidad.setEnabled(true);
            jAdimCita.setEnabled(true);
            jRegistarCita.setEnabled(true);
            jAdmConsulta.setEnabled(true);
            jConsultaMedica.setEnabled(true);
            jDiagnostico.setEnabled(true);
            jExamen.setEnabled(true);
            jReceta.setEnabled(true);
            jAdimHistorial.setEnabled(true);
            jCrearHistorialPaciente.setEnabled(true);
        }

        if (tipo == 2) {
            JAdministrarPersona.setEnabled(true);
            jRegistroPaciente.setEnabled(true);
            jRegistroAdim.setEnabled(false);
            jRegistroPersonal.setEnabled(false);
            jRegistroEspecialidad.setEnabled(false);
            jAdimCita.setEnabled(true);
            jRegistarCita.setEnabled(true);
            jAdmConsulta.setEnabled(true);
            jConsultaMedica.setEnabled(true);
            jDiagnostico.setEnabled(true);
            jExamen.setEnabled(true);
            jReceta.setEnabled(true);
            jAdimHistorial.setEnabled(true);
            jCrearHistorialPaciente.setEnabled(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jTextField1 = new javax.swing.JTextField();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        JAdministrarPersona = new javax.swing.JMenu();
        jRegistroPaciente = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jRegistroAdim = new javax.swing.JMenuItem();
        jRegistroPersonal = new javax.swing.JMenuItem();
        jRegistroEspecialidad = new javax.swing.JMenuItem();
        jVerEspecialidad = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jAdimCita = new javax.swing.JMenu();
        jRegistarCita = new javax.swing.JMenuItem();
        jAdmConsulta = new javax.swing.JMenu();
        jConsultaMedica = new javax.swing.JMenuItem();
        jExamen = new javax.swing.JMenuItem();
        jDiagnostico = new javax.swing.JMenuItem();
        jReceta = new javax.swing.JMenuItem();
        jAdimHistorial = new javax.swing.JMenu();
        jCrearHistorialPaciente = new javax.swing.JMenuItem();
        jhistorialPaciente = new javax.swing.JMenuItem();
        jSalirrr = new javax.swing.JMenu();
        salirSistema = new javax.swing.JMenuItem();
        cerrarSesion = new javax.swing.JMenuItem();

        jMenuItem4.setText("jMenuItem4");

        jMenuItem7.setText("jMenuItem7");

        jTextField1.setText("jTextField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(131, 161, 171));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, -10, 350, 730));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/solca_1.png"))); // NOI18N
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 700, 720));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 0, 730, 730));

        jPanel3.setBackground(new java.awt.Color(131, 161, 171));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 310, 730));

        JAdministrarPersona.setText("Registro");
        JAdministrarPersona.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jRegistroPaciente.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jRegistroPaciente.setText("Registro de Pacientes");
        jRegistroPaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRegistroPacienteActionPerformed(evt);
            }
        });
        JAdministrarPersona.add(jRegistroPaciente);

        jMenu1.setText("Registro del Personal");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jRegistroAdim.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jRegistroAdim.setText("Registro Administrador");
        jRegistroAdim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRegistroAdimActionPerformed(evt);
            }
        });
        jMenu1.add(jRegistroAdim);

        jRegistroPersonal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jRegistroPersonal.setText("Registro de Personal");
        jRegistroPersonal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRegistroPersonalActionPerformed(evt);
            }
        });
        jMenu1.add(jRegistroPersonal);

        jRegistroEspecialidad.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jRegistroEspecialidad.setText("Registro de Especialidad");
        jRegistroEspecialidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRegistroEspecialidadActionPerformed(evt);
            }
        });
        jMenu1.add(jRegistroEspecialidad);

        jVerEspecialidad.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jVerEspecialidad.setText("Ver Especialidades");
        jVerEspecialidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jVerEspecialidadActionPerformed(evt);
            }
        });
        jMenu1.add(jVerEspecialidad);

        JAdministrarPersona.add(jMenu1);
        JAdministrarPersona.add(jSeparator1);

        jMenuBar1.add(JAdministrarPersona);

        jAdimCita.setText("Adm.Cita");
        jAdimCita.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jRegistarCita.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jRegistarCita.setText("Agendar Cita ");
        jRegistarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRegistarCitaActionPerformed(evt);
            }
        });
        jAdimCita.add(jRegistarCita);

        jMenuBar1.add(jAdimCita);

        jAdmConsulta.setText("Adim.Consulta");
        jAdmConsulta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jConsultaMedica.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jConsultaMedica.setText("Consulta Médica");
        jConsultaMedica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jConsultaMedicaActionPerformed(evt);
            }
        });
        jAdmConsulta.add(jConsultaMedica);

        jExamen.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jExamen.setText("Examen");
        jExamen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jExamenActionPerformed(evt);
            }
        });
        jAdmConsulta.add(jExamen);

        jDiagnostico.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jDiagnostico.setText("Diagnostico");
        jDiagnostico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jDiagnosticoActionPerformed(evt);
            }
        });
        jAdmConsulta.add(jDiagnostico);

        jReceta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jReceta.setText("Receta");
        jReceta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRecetaActionPerformed(evt);
            }
        });
        jAdmConsulta.add(jReceta);

        jMenuBar1.add(jAdmConsulta);

        jAdimHistorial.setText("Historial Paciente");
        jAdimHistorial.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jCrearHistorialPaciente.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jCrearHistorialPaciente.setText("Crear Historial");
        jCrearHistorialPaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCrearHistorialPacienteActionPerformed(evt);
            }
        });
        jAdimHistorial.add(jCrearHistorialPaciente);

        jhistorialPaciente.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jhistorialPaciente.setText("Visualizar Historial");
        jhistorialPaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jhistorialPacienteActionPerformed(evt);
            }
        });
        jAdimHistorial.add(jhistorialPaciente);

        jMenuBar1.add(jAdimHistorial);

        jSalirrr.setText("Salir");
        jSalirrr.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        salirSistema.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        salirSistema.setText("Salir");
        salirSistema.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirSistemaActionPerformed(evt);
            }
        });
        jSalirrr.add(salirSistema);

        cerrarSesion.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cerrarSesion.setText("Cerrar Sesion");
        cerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesionActionPerformed(evt);
            }
        });
        jSalirrr.add(cerrarSesion);

        jMenuBar1.add(jSalirrr);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jRegistroPacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRegistroPacienteActionPerformed
        // TODO add your handling code here:
        Frm_Persona perso = new Frm_Persona();
        perso.setDefaultCloseOperation(HIDE_ON_CLOSE);
        perso.setVisible(true);
    }//GEN-LAST:event_jRegistroPacienteActionPerformed

    private void jRegistroPersonalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRegistroPersonalActionPerformed
        Frm_Medico medico = new Frm_Medico();
        medico.setDefaultCloseOperation(HIDE_ON_CLOSE);
        medico.setVisible(true);
    }//GEN-LAST:event_jRegistroPersonalActionPerformed

    private void jConsultaMedicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jConsultaMedicaActionPerformed
        Frm_ConsultaMedica consulta = new Frm_ConsultaMedica();
        consulta.setDefaultCloseOperation(HIDE_ON_CLOSE);
        consulta.setVisible(true);
    }//GEN-LAST:event_jConsultaMedicaActionPerformed

    private void salirSistemaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirSistemaActionPerformed
        System.exit(0);
    }//GEN-LAST:event_salirSistemaActionPerformed

    private void jRegistarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRegistarCitaActionPerformed
        Frm_Cita cita = new Frm_Cita();
        cita.setDefaultCloseOperation(HIDE_ON_CLOSE);
        cita.setVisible(true);
    }//GEN-LAST:event_jRegistarCitaActionPerformed

    private void jhistorialPacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jhistorialPacienteActionPerformed
        Frm_HistorialPaciente histo = new Frm_HistorialPaciente();
        histo.setDefaultCloseOperation(HIDE_ON_CLOSE);
        histo.setVisible(true);
    }//GEN-LAST:event_jhistorialPacienteActionPerformed

    private void jCrearHistorialPacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCrearHistorialPacienteActionPerformed
        Frm_crearHistorialPaciente crearHisto = new Frm_crearHistorialPaciente();
        crearHisto.setDefaultCloseOperation(HIDE_ON_CLOSE);
        crearHisto.setVisible(true);

//        Frm_VerActualizarHistorialPaciente crearHisto = new Frm_VerActualizarHistorialPaciente(this,"");
//        crearHisto.setDefaultCloseOperation(HIDE_ON_CLOSE);
//        crearHisto.setVisible(true);

    }//GEN-LAST:event_jCrearHistorialPacienteActionPerformed

    private void cerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesionActionPerformed
        Frm_Login se = new Frm_Login();
        this.dispose();
        se.setVisible(true);
    }//GEN-LAST:event_cerrarSesionActionPerformed

    private void jRecetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRecetaActionPerformed
        Frm_Receta recetita = new Frm_Receta();
        recetita.setDefaultCloseOperation(HIDE_ON_CLOSE);
        recetita.setVisible(true);
    }//GEN-LAST:event_jRecetaActionPerformed

    private void jRegistroEspecialidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRegistroEspecialidadActionPerformed
        Frm_CrearEspecialidad especialidad = new Frm_CrearEspecialidad();
        especialidad.setDefaultCloseOperation(HIDE_ON_CLOSE);
        especialidad.setVisible(true);
    }//GEN-LAST:event_jRegistroEspecialidadActionPerformed

    private void jRegistroAdimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRegistroAdimActionPerformed
        Frm_Administrador administrador = new Frm_Administrador();
        administrador.setDefaultCloseOperation(HIDE_ON_CLOSE);
        administrador.setVisible(true);
    }//GEN-LAST:event_jRegistroAdimActionPerformed

    private void jDiagnosticoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jDiagnosticoActionPerformed
        Frm_Diagnostico diagnostico = new Frm_Diagnostico();
        diagnostico.setDefaultCloseOperation(HIDE_ON_CLOSE);
        diagnostico.setVisible(true);
    }//GEN-LAST:event_jDiagnosticoActionPerformed

    private void jExamenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jExamenActionPerformed
        Frm_Examen examen = new Frm_Examen();
        examen.setDefaultCloseOperation(HIDE_ON_CLOSE);
        examen.setVisible(true);
    }//GEN-LAST:event_jExamenActionPerformed

    private void jVerEspecialidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jVerEspecialidadActionPerformed
        Frm_EspecialidadMedico especialidad = new Frm_EspecialidadMedico();
        especialidad.setDefaultCloseOperation(HIDE_ON_CLOSE);
        especialidad.setVisible(true);
    }//GEN-LAST:event_jVerEspecialidadActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_Principal.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_Principal.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_Principal.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_Principal.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new frmPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu JAdministrarPersona;
    private javax.swing.JMenuItem cerrarSesion;
    private javax.swing.JMenu jAdimCita;
    private javax.swing.JMenu jAdimHistorial;
    private javax.swing.JMenu jAdmConsulta;
    private javax.swing.JMenuItem jConsultaMedica;
    private javax.swing.JMenuItem jCrearHistorialPaciente;
    private javax.swing.JMenuItem jDiagnostico;
    private javax.swing.JMenuItem jExamen;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JMenuItem jReceta;
    private javax.swing.JMenuItem jRegistarCita;
    private javax.swing.JMenuItem jRegistroAdim;
    private javax.swing.JMenuItem jRegistroEspecialidad;
    private javax.swing.JMenuItem jRegistroPaciente;
    private javax.swing.JMenuItem jRegistroPersonal;
    private javax.swing.JMenu jSalirrr;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JMenuItem jVerEspecialidad;
    private javax.swing.JMenuItem jhistorialPaciente;
    private javax.swing.JMenuItem salirSistema;
    // End of variables declaration//GEN-END:variables
}
