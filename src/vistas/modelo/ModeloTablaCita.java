/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.modelo;

import controller.lista.ListaSimple;
import javax.swing.table.AbstractTableModel;
import modelo.Cita;
/**
 *
 * @author Anthony Luzuriaga
 */
public class ModeloTablaCita extends AbstractTableModel {

    private ListaSimple<Cita> lista;

    public ListaSimple<Cita> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<Cita> lista) {
        this.lista = lista;
    }
    

    @Override
    public int getColumnCount() {
        return 10;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    /**
     * El siguiete método permite obtener el contenido de la tabla
     *
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cita citita = lista.consultarDatoPos(rowIndex);
        switch (columnIndex) {
            case 0:
                return citita.getMedico().getNombre();
            case 1:
                return citita.getMedico().getApellido();
            case 2:
                return citita.getMedico().getEspecialidad().getTipo();
            case 3:
                return citita.getMedico().getCelular();
            case 4:
                return citita.getPersona().getNombre();
            case 5:
                return citita.getPersona().getApellido();
            case 6:
                return citita.getPersona().getCedula();
            case 7:
                return citita.getPersona().getCelular();
            case 8:
                return citita.getFecha();
            case 9:
                return citita.getHora();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int i) {
        switch (i) {
            case 0:
                return "Nombre Médico";
            case 1:
                return "Apellido Médico";
            case 2:
                return "Especialidad Médico";
            case 3:
                return "Celular Médico";
            case 4:
                return "Nombre Paciente";
            case 5:
                return "Apellido Paciente";
            case 6:
                return "Cedula Paciente";
            case 7:
                return "Celular";
            case 8:
                return "Fecha";
            case 9:
                return "Hora";
            default:
                return null;
        }
    }
}
