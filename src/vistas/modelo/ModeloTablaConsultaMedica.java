package vistas.modelo;

import controller.lista.ListaSimple;
import javax.swing.table.AbstractTableModel;
import modelo.ConsultaMedica;

/**
 *
 * @author Angel Román
 */
public class ModeloTablaConsultaMedica extends AbstractTableModel {
    private ListaSimple<ConsultaMedica> lista;

    public ListaSimple<ConsultaMedica> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<ConsultaMedica> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        ConsultaMedica consulta = lista.consultarDatoPos(i);
        switch(i1) {
            case 0: return consulta.getId();
            case 1: return consulta.getFecha();
            case 2: return consulta.getNroHistoria();
            case 3: return consulta.getCedulaMedico();
            case 4: return consulta.getTemperatura();
            case 5: return consulta.getPresionArterial();
            case 6: return consulta.getSaturacionOxigeno();
            case 7: return consulta.getMotivoConsulta();
            default: return null;
        }
    }
    
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0: return "Id";
            case 1: return "Fecha";
            case 2: return "Nro de Historia Clinica";   
            case 3: return "Cedula medico";
            case 4: return "Temperatura";
            case 5: return "Presion arterial";
            case 6: return "Saturacion de oxigeno";
            case 7: return "Motivo de consulta";
            default: return null;
        }
    }
}
