package vistas.modelo;

import controller.lista.ListaSimple;
import javax.swing.table.AbstractTableModel;
import modelo.Diagnostico;

/**
 *
 * @author Angel Román
 */
public class ModeloTablaDiagnostico extends AbstractTableModel {
    private ListaSimple<Diagnostico> lista;

    public ListaSimple<Diagnostico> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<Diagnostico> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Diagnostico diagnostico = lista.consultarDatoPos(i);
        switch(i1) {
            case 0: return diagnostico.getId();
            case 1: return diagnostico.getNroConsulta();
            case 2: return diagnostico.getDescripcion();
            default: return null;
        }
    }
    
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0: return "Id";
            case 1: return "Nro Consulta medica";    
            case 2: return "Descripcion";
            default: return null;
        }
    }
}
