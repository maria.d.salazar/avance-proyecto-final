
package vistas.modelo;

import controller.lista.ListaSimple;
import javax.swing.table.AbstractTableModel;
import modelo.Especialidad;

/**
 * @author Anthony luzuriaga
 */
public class ModeloTablaEspecialidad extends AbstractTableModel {
    private ListaSimple<Especialidad> lista;

    public ListaSimple<Especialidad> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<Especialidad> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Especialidad hp = lista.consultarDatoPos(i);
        switch(i1) {
            case 0: return hp.getTipo();
            default: return null;
        }
    }
    
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0: return "Nombre especialidad";
            default: return null;
        }
    }
    
}
