package vistas.modelo;

import controller.lista.ListaSimple;
import javax.swing.table.AbstractTableModel;
import modelo.Especialidad;
import modelo.Persona;

/**
 * @author Anthony luzuriaga
 */
public class ModeloTablaEspecialidadMedicos extends AbstractTableModel {

    private ListaSimple<Persona> persona;
    
    public ListaSimple<Persona> getPersona() {
        return persona;
    }

    public void setPersona(ListaSimple<Persona> persona) {
        this.persona = persona;
    }
    
    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public int getRowCount() {
        return persona.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Persona hp = persona.consultarDatoPos(i);
        switch (i1) {
//            case 0: return (i+1);
//            case 0:
////                return hp.getId();
//                return (i+1);
            case 0:
                return hp.getNombre();
            case 1:
                return hp.getApellido();
            case 2:
                if (hp.getEspecialidad() == null) {
                    hp.setEspecialidad(new Especialidad());
                }
                return hp.getEspecialidad().getTipo();
                
            case 3:
                return hp.getCelular();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int i) {
        switch (i) {
            case 0:
                return "Nombre";
            case 1:
                return "Apellido";
            case 2:
                return "Especialidad";
            case 3:
                return "Celular";
            default:
                return null;
        }
    }

}
