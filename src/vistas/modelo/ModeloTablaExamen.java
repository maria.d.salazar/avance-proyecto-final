package vistas.modelo;

import controller.lista.ListaSimple;
import javax.swing.table.AbstractTableModel;
import modelo.Examen;

/**
 *
 * @author Angel Román
 */
public class ModeloTablaExamen extends AbstractTableModel {
    private ListaSimple<Examen> lista;

    public ListaSimple<Examen> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<Examen> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Examen examen = lista.consultarDatoPos(i);
        switch(i1) {
            case 0: return examen.getId();
            case 1: return examen.getNroConsulta();
            case 2: return examen.getFecha();
            case 3: return examen.getTipo();
            case 4: return examen.getNombre();
            case 5: return examen.getDescripcion();
            default: return null;
        }
    }
    
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0: return "Id";
            case 1: return "Nro de Consulta Medica";
            case 2: return "Fecha";    
            case 3: return "Tipo";
            case 4: return "Nombre";
            case 5: return "Descripcion";
            default: return null;
        }
    }
}
