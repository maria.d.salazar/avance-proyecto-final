
package vistas.modelo;

import controller.lista.ListaSimple;
import javax.swing.table.AbstractTableModel;
import modelo.HistorialPaciente;

/**
 * @author Anthony luzuriaga
 */
public class ModeloTablaHistorialPaciente extends AbstractTableModel {
    private ListaSimple<HistorialPaciente> lista;

    public ListaSimple<HistorialPaciente> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<HistorialPaciente> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        HistorialPaciente hp = lista.consultarDatoPos(i);
        switch(i1) {
            case 0: return hp.getId();
            case 1: return hp.getNroDeHistoriaClinica();
            case 2: return hp.getPaciente().getNombre();
            case 3: return hp.getPaciente().getApellido();
            case 4: return hp.getGrupoSanguineo();
            case 5: return hp.getHabito();
            case 6: return hp.getAlergias();
            case 7: return hp.getEnfermedad();
            case 8: return hp.getEnfermedadHereditaria();
            default: return null;
        }
    }
    
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0: return "Id";
            case 1: return "NroHistoriaClinica";    
            case 2: return "Nombre";
            case 3: return "Apellido";
            case 4: return "Grupo Sanguineo";
            case 5: return "Habitos";
            case 6: return "Alergias";
            case 7: return "Enfermedad";
            case 8: return "Enfermedad Hereditaria";
            default: return null;
        }
    }
    
}
