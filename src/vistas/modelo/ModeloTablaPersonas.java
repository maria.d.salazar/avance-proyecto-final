/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.modelo;

import controller.lista.ListaSimple;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import modelo.Persona;

/**
 *
 * @author Mary Salazar
 */
public class ModeloTablaPersonas extends AbstractTableModel {

    private ListaSimple<Persona> lista;
    
    /**
     * Constructor vacio
     * @return 
     */
    public ListaSimple<Persona> getLista() {
        return lista;
    }

    /**
     * Constructor que recibe una lista de tipo ListaSimple "Persona"
     *
     * @param lista
     */
    public void setLista(ListaSimple<Persona> lista) {
        this.lista = lista;
    }

    /**
     * @return numero de columnas 
     */
    @Override
    public int getColumnCount() {
        return 12;
    }

    /**
     * @return numero de filas de acuerdo al tamanio de la lista
     */
    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    /**
     * El siguiete método permite obtener el contenido de la tabla
     *
     * @param i
     * @param i1
     * @return
     */
    @Override
    public Object getValueAt(int i, int i1) {
        Persona persona = lista.consultarDatoPos(i);

        switch (i1) {
            case 0:
                return persona.getCedula();
            case 1:
                return persona.getApellido();
            case 2:
                return persona.getNombre();
            case 3:
                return persona.getFechaNacimiento();  
            case 4:
                return persona.getEdad();
            case 5:
                return persona.getDireccion();
            case 6:
                return persona.getCelular();
            case 7:
                return persona.getCorreo();
            case 8:
                return persona.getNacionalidad();
            case 9:
                return persona.getCiudad();
            case 10:
                return persona.getProvincia();
            case 11:
                return persona.getSexo();
            case 12:
                return persona.getDiscapcidad();
            default:
                return null;
        }
    }

    /**
     * @param i
     * @return el nombre de cada columna
     */
    @Override
    public String getColumnName(int i) {
        switch (i) {
            case 0:
                return "Cedula";
            case 1:
                return "Apellido";
            case 2:
                return "Nombre";
            case 3:
                return "Fecha de Nacimiento";
            case 4:
                return "Edad";
            case 5:
                return "Direccion";
            case 6:
                return "Celular";
            case 7:
                return "Correo";
            case 8:
                return "Nacionalidad";
            case 9:
                return "Ciudad";
            case 10:
                return "Provincia";
            case 11:
                return "Sexo";
            case 12:
                return "Discapacidad";
            default:
                return null;
        }
    }

    /**
     * El siguiete método permite ajustar la tabla al contenido que tiene cada
     * celda
     *
     * @param tabla
     */
    public void ajustarTabla(JTable tabla) {
        tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < tabla.getColumnCount(); i++) {
            DefaultTableColumnModel colModelo = (DefaultTableColumnModel) tabla.getColumnModel();
            TableColumn col = colModelo.getColumn(i);
            int ancho = 0;

            TableCellRenderer renderizador = col.getHeaderRenderer();
            for (int j = 0; j < tabla.getRowCount(); j++) {
                renderizador = tabla.getCellRenderer(j, i);
                Component comp = renderizador.getTableCellRendererComponent(tabla, tabla.getValueAt(j, i), false, false, j, i);
                ancho = Math.max(ancho, comp.getPreferredSize().width);
            }
            col.setPreferredWidth(ancho + 2);
        }
    }
}
