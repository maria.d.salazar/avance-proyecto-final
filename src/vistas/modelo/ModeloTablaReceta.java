package vistas.modelo;

import controller.lista.ListaSimple;
import javax.swing.table.AbstractTableModel;
import modelo.Receta;

/**
 * @author Anthony luzuriaga
 */
public class ModeloTablaReceta extends AbstractTableModel {

    private ListaSimple<Receta> lista;

    public ListaSimple<Receta> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<Receta> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Receta rec = lista.consultarDatoPos(i);
        switch (i1) {
            case 0:
                return rec.getNombre();
            case 1:
                return rec.getFechita();
            case 2:
                return rec.getPosologia();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int i) {
        switch (i) {
            case 0:
                return "Nombre";
            case 1:
                return "Fecha";
            case 2:
                return "Posologia";
            default:
                return null;
        }
    }

}
